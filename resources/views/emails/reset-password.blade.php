@extends('layouts.email')

@section('content')
<h2>Reset your password on getionpay.com</h2>

<p>We got a reset password request on your account with getionpay. click on the link below to reset your password</p>

<p><a href="{{$url}}">{{$url}}</a></p>

<p>If you did not request for a password request, please contact us immediately.</p>

@endsection