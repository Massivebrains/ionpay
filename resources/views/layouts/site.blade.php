<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Ionepay - Conversational Commerce</title>
	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">
	<link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon" />
	<link rel="apple-touch-icon" href="/img/apple-touch-icon.png">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
	<link href="https:/fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/vendor/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="/vendor/animate/animate.min.css">
	<link rel="stylesheet" href="/vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" href="/vendor/magnific-popup/magnific-popup.min.css">
	<link rel="stylesheet" href="/css/theme.css">
	<link rel="stylesheet" href="/css/theme-elements.css">
	<link rel="stylesheet" href="/css/demo-landing.css">
	<link rel="stylesheet" href="/css/skin-landing.css">
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/custom.css">
	<script src="/vendor/modernizr/modernizr.min.js"></script>

</head>
<body class="alternative-font-4 loading-overlay-showing" data-plugin-page-transition data-loading-overlay data-plugin-options="{'hideDelay': 500}">
	<div class="loading-overlay">
		<div class="bounce-loader">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div>

	<div class="body">

		@include('components.header')

		<div role="main" class="main">

			@yield('content')

			@include('components.footer')

		</div>

	</div>

    <script>
        function forPasswordEye() {
            var myPassword = document.getElementById('cpassword');
            var myEye = document.getElementById('myEye');

            if(myPassword.type === 'password'){
                myPassword.type = 'text';
                myEye.classList.add("fa-eye-slash")
                myEye.classList.remove("fa-eye")

            }else{
                myPassword.type = 'password';
                myEye.classList.add("fa-eye")
                myEye.classList.remove("fa-eye-slash")
            }
        }
    </script>
	<script src="/vendor/jquery/jquery.min.js"></script>
	<script src="/vendor/jquery.appear/jquery.appear.min.js"></script>
	<script src="/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="/vendor/jquery.cookie/jquery.cookie.min.js"></script>
	<script src="/vendor/popper/umd/popper.min.js"></script>
	<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="/vendor/common/common.min.js"></script>
	<script src="/vendor/vide/jquery.vide.min.js"></script>
	<script src="/vendor/vivus/vivus.min.js"></script>
	<script src="/js/theme.js"></script>
	<script src="/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="/js/custom.js"></script>
	<script src="/js/theme.init.js"></script>

</body>
</html>
