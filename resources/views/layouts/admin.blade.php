<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Ionpay - Conversational Commerce">
    <meta name="keywords" content="Ionpay, Conversation, Commerce">
    <link href="https:/fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https:/cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="author" content="Olaiya Segun <vadeshayo@gmail.com>">
    <title>Ionepay Admin</title>
    <link rel="icon" type="image/png" href="favicon.png">
    <link href="https:/fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/admin-assets/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/admin-assets/css/app.min.css">
    <link rel="stylesheet" type="text/css" href="/admin-assets/css/core/menu/menu-types/vertical-menu-modern.css">
    <link rel="stylesheet" type="text/css" href="/admin-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="/admin-assets/css/trumbowyg.min.css">
    <link rel="stylesheet" type="text/css" href="/admin-assets/css/style.css">
    @yield('styles')
</head>

<body class="vertical-layout vertical-menu-modern 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

    <style>
        .ft-toggle-right:before{
            content: url('/admin-assets/images/controls/sidebar-button.svg') !important;
        }
    </style>

    @include('components.nav')

    @yield('content')

    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span class="float-md-left d-block d-md-inline-block">© 2020 
                <a class="text-bold-800 grey darken-2" href="#" target="_blank">iOnePay.</a> 
                All Rights Reserved 
            </span>
        </p>
    </footer>

    <script src="/admin-assets/js/vendors.min.js" type="text/javascript"></script>
    <script src="/admin-assets/js/app-menu.min.js" type="text/javascript"></script>
    <script src="/admin-assets/js/app.min.js" type="text/javascript"></script>
    <script src="/admin-assets/js/customizer.min.js" type="text/javascript"></script>
    <script src="/admin-assets/js/trumbowyg.min.js"></script>
    <script src="/admin-assets/js/delete.js"></script>


</body>
</html>
