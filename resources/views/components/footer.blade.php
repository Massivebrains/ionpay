<footer>
	<div class="container">

		<div class="row py-5 my-4 border-top-dark">
			<div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
				<h5 class="text-3 mb-3"><img src="/img/controls/logo-footer.svg" /></h5>
				<p>A Conversational E-commerce</p>
				<p class="pr-1">14A Oba Elegushi Road, <br>Ikoyi, Lagos Nigeria. <br>
					+234 80 0000 0000
				</p>

			</div>
			<div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
				<h5 class="text-3 mb-3">Quick Links</h5>
				<p><a href="/about" class="footer-links">About Us</a></p>
				<p><a href="/#how-it-works" class="footer-links">How It Works</a></p>
				<p><a href="/#our-channels" class="footer-links">Our Channels</a></p>
			</div>
			<div class="col-md-6 col-lg-4 mb-4 mb-md-0">
				<div class="contact-details">
					<h5 class="text-3 mb-3">Connect</h5>
					<p><a href="/login" class="footer-links">Login</a></p>
					<p><a href="/connect#faq" class="footer-links">FAQ</a></p>
					<p><a href="/connect" class="footer-links">Contact Us</a></p>
					<p><a href="/careers" class="footer-links">Careers</a></p>

				</div>
			</div>
{{--			<div class="col-md-6 col-lg-4">--}}
{{--				<h5 class="text-3 mb-3">Newsletter</h5>--}}
{{--				<p>Get monthly tips and updates and keep up with the iOnePay team!</p>--}}
{{--				<form>--}}
{{--					<div class="row">--}}
{{--						<div class="col-lg-12">--}}
{{--							<input type="text" class="form-control customise long pull-left" style="width:100%; border-radius: 4px; border-color: #BCD0E5; background-color: #FFFFFF" placeholder="Enter your email address">--}}
{{--							<button class="btn btn-danger mt-2" style="float: left; padding: 15px 40px; border-radius: 4px">Subscribe</button>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</form>--}}
{{--			</div>--}}
		</div>
	</div>

</footer>
<section class="section  section-concept sect section-no-border section-dark section-angled section-angled-reverse pt-5 m-0 last" >

	<div class="container text-left last-footer">
		<p class="mt-2">
			<a href="/terms-and-condition" class="footer-links mr-5"><b>Terms & Conditions</b></a>
			<a href="/privacy-policy" class="footer-links">Privacy Policy</a>
		</p>
		<p class="text-4  align-self-center">&copy; 2020 iOnePay Nigeria. All Rights Reserved</p>
	</div>

</section>
