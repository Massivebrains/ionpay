@php
   $active = isset($active) ? $active : '';
@endphp

<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark navbar-shadow">
   <div class="navbar-wrapper">
       <div class="navbar-header">
           <ul class="nav navbar-nav flex-row">
               <li class="nav-item mr-auto">
                   <a class="navbar-brand" href="">
                       <img class="brand-logo" alt="modern admin logo" src="/admin-assets/images/logo/logo-small.svg">
                       <img class="brand-logo expanded" alt="modern admin logo" width="131" height="38" src="/admin-assets/images/logo/logo.svg">
                   </a>
               </li>
               <li class="nav-item mobile-menu d-md-none ml-auto" style="align-self: center"><a class="nav-link nav-link-label hidden-xs" href="#"><i class="ficon ft-bell active"></i></a></li>
               <li class="nav-item mobile-menu d-md-none"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
               <li class="nav-item d-none d-md-block float-right"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="toggle-icon ft-toggle-right font-medium-3 white"></i></a></li>
               <li class="nav-item d-none d-md-none">
                   <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
               </li>
           </ul>
       </div>

       <div class="navbar-container content">
           <div class="collapse navbar-collapse" id="navbar-mobile">

               <ul class="nav navbar-nav mr-auto float-left" style="width: 80%">

                   <li class="nav-item nav-search" style="width: 100%">
                       <div class="search-bar">
                           <i class="fa fa-search"></i>
                           <input class="input" type="text" placeholder="Search by keywords…" tabindex="0"style="height: 100%; padding: 10px; flex-grow: 1" >
                       </div>

                   </li>
               </ul>

               <ul class="nav navbar-nav float-right">
                   <li class="dropdown dropdown-notification nav-item">
                       <a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                           <i class="ficon ft-bell active"></i></a>
                           <div class="dropdown-menu dropdown-menu-right" style="border-radius: 0;">
                               <a class="dropdown-item" href="#">You have 0 new notification</a>
                               <div class="dropdown-divider"></div>
                               <a class="dropdown-item" href="#"> See all notifications</a>
                           </div>
                       </li>
                       <li class="dropdown dropdown-user nav-item">
                           <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                               <span class="avatar avatar-online"><img src="/admin-assets/images/avatar.png" alt="avatar"></span>
                           </a>

                       </li>
                   </ul>

               </div>
           </div>
       </div>
   </nav>


   <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
       <div class="main-menu-content">


           <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
               <li class="nav-item nav-search d-lg-none " style="width: 95%">
                   <div class="search-bar">
                       <i class="fa fa-search" style="align-self: center;"></i>
                       <input class="input" type="text" placeholder="Search by keywords…" tabindex="0" style="height: 100%;padding: 10px;flex-grow: 1;display: block;">
                   </div>

               </li>
               <li class="mt-lg-50">
                   <a href="/admin" class="nav-item {{$active == 'dashboard' ? 'active' : ''}}">
                       <i class="icon icon-dashboard"></i>
                       <span class="menu-title">Dashboard</span>
                   </a>
               </li>
               <hr>
               <li>
                   <a class="/products {{$active == 'products' ? 'active' : ''}}" href="/admin/products">
                       <i class="icon icon-services"></i>
                       <span class="menu-title">Services</span>
                   </a>
               </li>

               <li>
                   <a class="nav-item {{$active == 'orders' ? 'active' : ''}}" href="/admin/orders">
                       <i class="icon icon-transaction"></i>
                       <span class="menu-title">Orders</span>
                   </a>
               </li>


               <li>
                   <a class="nav-item {{$active == 'wallet' ? 'active' : ''}}" href="/admin/wallet">
                       <i class="icon icon-wallet "></i>
                       <span class="menu-title">Wallet</span>
                   </a>
               </li>

               @if(session('user')->type == 'premium')
               <li>
                   <a class="nav-item {{$active == 'merchants' ? 'active' : ''}}" href="/admin/merchants">
                       <i class="icon icon-customers"></i>
                       <span class="menu-title">Merchants</span>
                   </a>
               </li>
               @endif

               <li>
                   <a class="nav-item {{$active == 'customers' ? 'active' : ''}}" href="/admin/customers">
                       <i class="icon icon-customers"></i>
                       <span class="menu-title">Customers</span>
                   </a>
               </li>

               <li>
                   <a class="nav-item {{$active == 'subscribers' ? 'active' : ''}}" href="/admin/subscribers">
                       <i class="icon icon-subscribers "></i>
                       <span class="menu-title">Subscribers</span>
                   </a>
               </li>
               <li>
                   <a class="nav-item {{$active == 'account' ? 'active' : ''}}" href="/admin/account">
                       <i class="icon icon-account"></i>
                       <span class="menu-title">Account</span>
                   </a>
               </li>

               <hr>

               <li>
                   <a class="nav-item {{$active == 'settings' ? 'active' : ''}}" href="/admin/settings">
                       <i class="icon icon-settings"></i>
                       <span class="menu-title">Settings</span>
                   </a>
               </li>

               <li>
                   <a class="nav-item {{$active == 'helpe' ? 'active' : ''}}" href="/connect">
                       <i class="icon icon-help"></i>
                       <span class="menu-title">Help </span>
                   </a>
               </li>
               <li>
                   <a class="nav-item" href="/logout">
                       <i class="icon icon-logout"></i>
                       <span class="menu-title">Log Out</span>
                   </a>
               </li>

           </ul>
       </div>
   </div>
