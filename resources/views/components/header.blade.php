
@php
$background = isset($bg) ? $bg : '';
$active = isset($active) ? $active : 'home';
@endphp

<header id="header" class="header-transparent" data-plugin-options="{'stickyEnabled': false, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': false, 'stickyEnableOnMobile': true, 'stickyStartAt': 70, 'stickyChangeLogo': false, 'stickyHeaderContainerHeight': 70}">
	<div class="header-body border-top-0 bg-home box-shadow-none">
		<div class="header-container container {{$background}}">

			<div class="header-row">
				<div class="header-column">
					<div class="header-row">
						<div class="header-logo appear-animation" data-appear-animation="fadeIn" data-plugin-options="{'accY': 100}">
							<a href="/" class="goto-top btn-mobile-hide">
								<img alt="Porto" width="auto" height="52" data-sticky-width="82" data-sticky-height="36" data-sticky-top="0" src="/img/controls/logo.svg">
							</a>
							<a href="/" class="goto-top img-pc-hide">
								<img alt="Porto" width="auto" height="52" data-sticky-width="82" data-sticky-height="36" data-sticky-top="0" src="/img/controlv2/logo-mobile.svg">
							</a>
						</div>
					</div>
				</div>
				<div class="header-column justify-content-end">
					<div class="header-row">
						<div class="header-nav header-nav-links header-nav-light-text header-nav-dropdowns-dark">
							<div class="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-text-capitalize header-nav-main-text-size-2 header-nav-main-mobile-dark header-nav-main-effect-1 header-nav-main-sub-effect-1 appear-animation" data-appear-animation="fadeIn" data-plugin-options="{'accY': 100}">
								<nav class="collapse">
									<ul class="nav nav-pills" id="mainNav">
										<li class="dropdown">
											<a class="dropdown-item {{$active == 'home' ? 'active' : ''}}" href="/">
												Home
											</a>
										</li>

										<li class="dropdown">
											<a class="dropdown-item {{$active == 'about' ? 'active' : ''}}" data-hash data-hash-offset="130" href="/about">
												About
											</a>
										</li>
										<li class="dropdown">
											<a class="dropdown-item {{$active == 'careers' ? 'active' : ''}}" data-hash data-hash-offset="130" href="/careers">
												Careers
											</a>
										</li>
										<li class="dropdown">
											<a class="dropdown-item {{$active == 'connect' ? 'active' : ''}}" data-hash data-hash-offset="130" href="/connect">
												Connect
											</a>
										</li>
{{--										<li class="dropdown">--}}
{{--											<a class="dropdown-item {{$active == 'blog' ? 'active' : ''}}" data-hash data-hash-offset="130" href="/blog">--}}
{{--												Blog--}}
{{--											</a>--}}
{{--										</li>--}}

										<li class="dropdown block-pc-hide">
											<a class="dropdown-item {{$active == 'login' ? 'active' : ''}}" data-hash data-hash-offset="130" href="/login">
												Login
											</a>
										</li>



									</ul>
								</nav>
							</div>
{{--							<a class="btn-mobile-hide" href="/login" style="color: #173A56; text-align: right; margin-right: -50px;">--}}
{{--								<strong>Login</strong>--}}
{{--							</a>--}}
							<a class="btn  btn-danger font-weight-semibold px-4 ml-3 appear-animation btn-mobile-hide" data-appear-animation="fadeIn" data-plugin-options="{'accY': 100}" href="/login">LOGIN</a>
							<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav"><i class="fa fa-bars"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
