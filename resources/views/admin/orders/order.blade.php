@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" type="text/css" href="/admin-assets/css/vendors/transaction-detail.css">
@endsection


@section('content')

<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-header">
			<h2 class="title">Transactions</h2>
		</div>


		<div class="content-body ">

			<div class="card p-20">
				<div class="card-header head">
					<h4 class="diff-title">Order Information</h4>
					<a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

				</div>
				<div class="card-content row mt-1">
					<div class="col-sm-6 col-md-6 col-lg-6">
						<div class="row">

							<div class="form-group col-sm-12 col-md-12 col-lg-12">
								<label for="referenceNumber">Transaction Reference Number</label>
								<input type="text" value="{{$order->order_reference}}" disabled class="form-control  input-box b-white">
							</div>
							<div class="form-group col-sm-12 col-lg-12">
								<label for="paymentStatus">Payment Status</label><br>
								{{_badge($order->order_status)}} | {{strtoupper($order->payment_type)}}
							</div>
							<div class="form-group col-sm-12 col-lg-12">
								<label for="transactionTotal">Transaction Total</label>
								<input type="text" id="transactionTotal" value="{{_c($order->total)}}" disabled class="form-control  input-box b-white">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-sm-12 col-lg-12">
								<label for="transactionDate">Transaction Date</label>
								<input type="text" name="transactionDate" id="transactionDate" value="{{_d($order->date_ordered)}}" disabled class="form-control  input-box b-white">
							</div>
							<div class="form-group col-sm-12 col-lg-12">
								<label for="paymentDate">Payment Date</label>
								<input type="text" name="paymentDate" id="paymentDate" value="{{_d($order->payment_date)}}" disabled class="form-control  input-box b-white">
							</div>
							<div class="form-group col-sm-12 col-lg-12">
								<label for="customerPhoneNumber">Customer Phone Number</label>
								<input type="text" name="customerPhoneNumber" id="customerPhoneNumber" value="{{$order->msisdn}}" disabled class="form-control input-box b-white">
							</div>
							<div class="form-group col-sm-12 col-lg-12">
								<label for="shippingAddress"><i class="icon icon-shipping mr-2"></i> Shipping Address</label>
								<input type="text" name="shippingAddress" id="shippingAddress" value="{{$order->shipping_address}}" disabled class="form-control input-box b-white">
							</div>
						</div>

					</div>
					<div class="col-sm-6 col-md-6 col-lg-6">
						<h4 class="diff-title">Order Details</h4>
						<div class="row">

							<div class="col-lg-12 col-sm-12">
								<table class="table table-striped table-responsive">
									<thead>
										<tr>
											<th>CODE</th>
											<th>PRICE EACH</th>
											<th>QUANTITY</th>
											<th>PRICE TOTAL</th>
										</tr>
									</thead>
									<tbody>

										@foreach($order->order_details as $row)
										<tr>
											<td>{{$row->product->product_code}}</td>
											<td>{{$row->product->product_price}}</td>
											<td>{{$row->quantity}}</td>
											<td>{{(int)$row->product->product_price * (int)$row->quantity}}</td>
										</tr>
										@endforeach

									</tbody>
								</table>
							</div>

						</div>

					</div>

				</div>
			</div>

		</div>

	</div>
</div>

@endsection