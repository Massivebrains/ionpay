@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" type="text/css" href="/admin-assets/css/vendors/transactions.css">
@endsection

@section('content')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header">
            <h2 class="title">Orders</h2>
        </div>

        <div class="content-body">
          <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="card-header mb-3 p-0">
                        <div><h4 class="diff-title">Orders</h4></div>
                                    {{-- <div class="heading-elements pull-right">
                                        <ul class="list-inline mb-0" style="display: table;">
                                            <li class="pull-right" style="width: 250px;">
                                                <input  type="text" class="form-control input-custom"  placeholder="Search"/>
                                            </li>
                                        </ul>
                                    </div> --}}
                                </div>

                                <table class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>Reference</th>
                                            <th>Customer</th>
                                            <th>Order Date</th>
                                            <th>Payment Date</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orders as $row)
                                        <tr>
                                            <td>{{$row->order_reference}}</td>
                                            <td>{{$row->msisdn}}</td>
                                            <td>{{_d($row->date_ordered)}}</td>
                                            <td>{{_d($row->payment_date)}}</td>
                                            <td>{{_c($row->total)}}</td>
                                            <td>{{_badge($row->order_status)}}</td>
                                            <td><a href="/admin/order/{{$row->order_id}}" class="btn btn-outline-blue btn-sm">View Details</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>

        @endsection