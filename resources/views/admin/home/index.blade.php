@extends('layouts.admin')

@section('content')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header">
            <h2 class="title">Dashboard</h2>
        </div>


        <div class="content-body">
            <div class="row">

                <div class="col-sm-12 col-md-12 col-lg-12">

                    @if(session('user')->type == 'merchant')
                    <iframe src="https://bi.ogaranya.com/public/dashboard/2c6c5349-abff-4469-96ac-94b8f8f4ed95?merchant={{session('user')->merchant_id}}#hide_parameters=merchant" frameborder="0" width="100%" height="800" allowtransparency></iframe>
                    @else
                    <iframe src="https://bi.ogaranya.com/public/dashboard/e01858fe-6ffc-4e65-85ba-34ecccef4a67?merchant={{session('user')->merchant_id}}#hide_parameters=merchant" frameborder="0" width="100%" height="800" allowtransparency></iframe>
                    @endif
                    
                </div>
            </div>


        </div>
    </div>

    @endsection