@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" type="text/css" href="/admin-assets/css/vendors/others.css">
@endsection

@section('content')

<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-header">
			<h2 class="title">Recent Wallet Activities</h2>
		</div>


		<div class="content-body">
			
			<div class="card">
				<div class="card-body">
				<div class="card-header mb-3 p-0">
					<div><h4 class="diff-title">Recent Wallet Activities</h4></div>
				</div>


				<div class="heading div-tabled">
					<div class="cols text-left"><span>Transaction Date</span></div>
					<div class="cols text-left"><span>Description</span></div>
					<div class="cols text-left"><span>Type</span></div>
					<div class="cols text-left"><span>Amount</span></div>
				</div>


				@foreach($transactions as $row)
				<div class="div-tabled">
					<div class="cols text-left"><span>{{_d($row->created_at)}}</span></div>
					<div class="cols text-left"><span>{{$row->description}}</span></div>
					<div class="cols text-left"><span>{{$row->transaction_type == 'out' ? 'Debit' : 'Credit'}}</span></div>
					<div class="cols text-left"><span class="debit">{{_c($row->amount)}}</span></div>
				</div>
				@endforeach

			</div>
			</div>

		</div>



	</div>
</div>

@endsection