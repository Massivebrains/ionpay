@extends('layouts.admin')

@section('content')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header">
            <h2 class="title">{{$merchant->merchant_name ?? 'Add New Merchant'}}</h2>
        </div>

        @include('components.alert')

        <div class="content-body ">

            <div class="card p-20">

                <div class="card-content row mt-1">
                    <div class="col-sm-12 col-md-12 col-lg-12">

                        <form action="/admin/merchant/{{$merchant->id}}" method="POST">

                            @csrf
                            <div class="row">

                                <div class="form-group col-sm-12 col-lg-12">
                                    <label for="accountName">Name</label>
                                    <input type="text" value="{{$merchant->merchant_name}}" name="merchant_name" class="form-control input-box b-white" required>
                                </div>

                            </div>

                            <div class="row">

                                <div class="form-group col-sm-6 col-lg-6">
                                    <label for="transactionDate">Phone</label>
                                    <input type="number" name="merchant_phone" value="{{$merchant->merchant_phone}}" class="form-control input-box b-white" required>
                                </div>

                                <div class="form-group col-sm-6 col-lg-6">
                                    <label for="transactionDate">Emaill Address</label>
                                    <input type="email" name="merchant_email" value="{{$merchant->merchant_email}}" class="form-control input-box b-white" required>
                                </div>                                

                            </div>

                            <div class="row">

                                <div class="form-group col-sm-12 col-lg-12">
                                    <label for="transactionDate">Contact Person's Name</label>
                                    <input type="text" name="merchant_contact_person" value="{{$merchant->merchant_contact_person}}" class="form-control input-box b-white" required>
                                </div>

                                @if($merchant->id < 1)
                                
                                <div class="form-group col-sm-6 col-lg-6">
                                    <label for="transactionDate">Password</label>
                                    <input type="password" name="password" class="form-control input-box b-white" required>
                                </div> 

                                <div class="form-group col-sm-6 col-lg-6">
                                    <label for="transactionDate">Retype Password</label>
                                    <input type="password" name="password_confirmation" class="form-control input-box b-white" required>
                                </div> 
                                
                                @endif                               

                            </div>

                            <div class="row">
                                <div class="form-group col-sm-12 col-lg-12">
                                    <label for="paymentDate">Address</label>
                                    <textarea name="merchant_address" class="form-control" rows="3" required>{{$merchant->merchant_address}}</textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-sm-12 col-lg-12">
                                    <label for="paymentDate">Address</label>
                                    <select name="status" class="form-control" required>
                                        <option value="enabled" {{$merchant->status == 'enabled' ? 'selected' : ''}}>Enabled</option>
                                        <option value="disabled" {{$merchant->status == 'disabled' ? 'selected' : ''}}>Disabled</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-12">
                                    <input type="hidden" name="id" value="{{$merchant->id}}">
                                    <button type="submit" class="btn btn-blue">Save Merchant</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

@endsection