@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" type="text/css" href="/admin-assets/css/vendors/transactions.css">
@endsection

@section('content')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header">
            <h2 class="title">Merchants</h2>
        </div>

        @include('components.alert')

        <div class="content-body">
          <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="card-header mb-3 p-0">
                        <div>
                            <h4 class="diff-title">Merchants</h4>
                            <a href="/admin/merchant/0" class="btn btn-blue setbtn pull-right">Add New</a>
                        </div>
                    </div>

                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email Address</th>
                                <th>Phone</th>
                                <th>Wallet Balance</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($merchants as $row)
                            <tr>
                                <td>{{$row->merchant_name}}</td>
                                <td>{{$row->merchant_email}}</td>
                                <td>{{$row->merchant_phone}}</td>
                                <td>{{$row->wallet}}</td>
                                <td><a href="/admin/merchant/{{$row->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                </div>

            </div>
        </div>
    </div>

</div>
</div>

@endsection