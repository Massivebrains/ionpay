@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" type="text/css" href="/admin-assets/css/vendors/others.css">
@endsection

@section('content')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header">
            <h2 class="title">Services</h2>
        </div>

        @include('components.alert')

        <div class="content-body">
            <div class="">



                <div class="card-content ">
                    <div class="">
                        <ul class="nav nav-tabs nav-underline no-hover-bg">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab3" data-toggle="tab" href="#home3" aria-controls="home3" aria-expanded="true">Products</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#profile3" aria-controls="profile3" aria-expanded="false">Events</a>
                            </li>

                        </ul>
                        <div class="tab-content px-1 pt-1 card mt-30">
                            <div role="tabpanel" class="tab-pane active" id="home3" aria-labelledby="home-tab3" aria-expanded="true">

                                <div class="card-header mb-3 p-0">
                                    <div><h4 class="diff-title">Product Module</h4></div>
                                    <div class="heading-elements pull-right">
                                        <ul class="list-inline mb-0" style="display: table;">
                                            <li style="width: 250px;">
                                                <form type="GET">
                                                    <input  type="text" class="form-control input-custom"  placeholder="Search" name="query" />
                                                </form>
                                            </li>
                                            <li>
                                                <a class="btn btn-blue setbtn" href="/admin/product/0/product">Add New</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>



                                <div class="heading div-tabled">
                                    <div class="cols text-left"><span>Code</span></div>
                                    <div class="cols text-left"><span>Name</span></div>
                                    <div class="cols text-left"><span>Price</span></div>
                                    <div class="cols text-left"><span>Quantity</span></div>
                                    <div class="cols text-left"><span>Action</span></div>
                                </div>

                                @foreach($products as $row)
                                <div class="div-tabled">
                                    <div class="cols text-left"><span>{{$row->product_code}}</span></div>
                                    <div class="cols text-left"><span>{{$row->product_name}}</span></div>
                                    <div class="cols text-left"><span>{{_c($row->product_price)}}</span></div>
                                    <div class="cols text-left"><span>{{$row->quantity}}</span></div>
                                    <div class="cols text-left">
                                        <span>
                                            <a href="/admin/product/{{$row->id}}">
                                                <i class="icon icon-edit-button"></i> Edit
                                            </a>
                                        </span>
                                    </div>
                                </div>
                                @endforeach
                                
                            </div>
                            <div class="tab-pane " id="profile3" role="tabpanel" aria-labelledby="profile-tab3" aria-expanded="false">
                                <div class="card-header mb-3 p-0">
                                    <div><h4 class="diff-title">Event Module</h4></div>
                                    <div class="heading-elements pull-right" style="position: unset">
                                        <ul class="list-inline mb-0" style="display: table;">
                                            <li style="width: 250px;">
                                                <input  type="text" class="form-control input-custom"  placeholder="Search"/>
                                            </li>
                                            <li>
                                                <a class="btn btn-blue setbtn" href="/admin/product/0/event">Add New</a>

                                            </li>


                                        </ul>
                                    </div>
                                </div>
                                <div class="heading div-tabled">
                                    <div class="cols text-left"><span>Code</span></div>
                                    <div class="cols text-left"><span>Name</span></div>
                                    <div class="cols text-left"><span>Price</span></div>
                                    <div class="cols text-left"><span>Quantity</span></div>
                                    <div class="cols text-left"><span>Action</span></div>
                                </div>
                                @foreach($events as $row)
                                <div class="div-tabled">
                                    <div class="cols text-left"><span>{{$row->product_code}}</span></div>
                                    <div class="cols text-left"><span>{{$row->product_name}}</span></div>
                                    <div class="cols text-left"><span>{{_c($row->product_price)}}</span></div>
                                    <div class="cols text-left"><span>{{$row->quantity}}</span></div>
                                    <div class="cols text-left">
                                        <span>
                                            <a href="/admin/product/{{$row->id}}">
                                                <i class="icon icon-edit-button"></i> 
                                            </a>
                                            <a href="/admin/product/{{$row->id}}">
                                                <i class="icon icon-delete-button"></i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                            
                        </div>
                    </div>

                </div>


            </div>


        </div>



    </div>
</div>
@endsection