@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" type="text/css" href="/admin-assets/css/vendors/others.css">
@endsection

@section('content')

<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-header">
			<h2 class="title">Customers</h2>
		</div>

		<div class="content-body">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="card-header mb-3 p-0">
							<div><h4 class="diff-title">Customers</h4></div>
							<div class="heading-elements pull-right">
								<ul class="list-inline mb-0" style="display: table;">
									<li class="pull-right" style="width: 250px;">
										<input  type="text" class="form-control input-custom"  placeholder="Search"/>
									</li>
								</ul>
							</div>
						</div>

						<div class="heading div-tabled">
							<div class="sn">#</div>
							<div class="cols"><span>Customer Name</span></div>
							<div class="cols"><span>Customer Number</span></div>
							<div class="cols"><span>Customer Email</span></div>
							<div class="cols"><span>Date Joined</span></div>
						</div>

						@foreach($customers as $row)
						<div class="div-tabled">
							<div class="cols"><span>{{$row->name}}</span></div>
							<div class="cols"><span>{{$row->msisdn}}</span></div>
							<div class="cols"><span>{{$row->email}}</span></div>
							<div class="cols"><span>{{$row->date_added}}</span></div>
						</div>
						@endforeach

						@if(count($customers) < 1)
						<div class="heading div-tabled">
							<div class="cols" style='width:100%'><span>Your Customers will show up here.</span></div>
						</div>
						@endif
						

					</div>

				</div>
			</div>


		</div>



	</div>
</div>

@endsection