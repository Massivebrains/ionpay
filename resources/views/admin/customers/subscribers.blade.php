@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" type="text/css" href="/admin-assets/css/vendors/others.css">
@endsection

@section('content')

<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-header">
			<h2 class="title">Subscribers</h2>
		</div>

		<div class="content-body">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="card-header mb-3 p-0">
							<div><h4 class="diff-title">Subscribers</h4></div>
							<div class="heading-elements pull-right">
								<ul class="list-inline mb-0" style="display: table;">
									<li class="pull-right" style="width: 250px;">
										<input  type="text" class="form-control input-custom"  placeholder="Search"/>
									</li>
								</ul>
							</div>
						</div>

						<div class="heading div-tabled">
							<div class="sn">#</div>
							<div class="cols"><span>Phone Number</span></div>
							<div class="cols"><span>Info</span></div>
							<div class="cols "><span>Command</span></div>
							<div class="cols"><span>Date</span></div>
						</div>

						@foreach($subscribers as $row)
						<div class="div-tabled">
							<div class="cols">{{$row->msisdn}}</div>
							<div class="cols">{{$row->log}}</div>
							<div class="cols">{{$row->command->custom_command}}</div>
							<div class="cols">{{$row->created_at}}</div>
						</div>
						@endforeach

						@if(count($subscribers) < 1)
						<div class="div-tabled">
							<div class="cols" style="width:100%">Your Subscribers will show up here.</div>
						</div>
						@endif

					</div>

				</div>
			</div>


		</div>

	</div>
</div>

@endsection