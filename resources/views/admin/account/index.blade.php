@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" type="text/css" href="/admin-assets/css/vendors/others.css">
@endsection

@section('content')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header">
            <h2 class="title">Account</h2>
        </div>

        @include('components.alert')

        <div class="content-body">

            <div class="card p-20">
                <div class="card-header head">
                    <h4 class="diff-title">Merchant Bank Details</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                </div>
                <div class="card-content row mt-1">

                    <form action="/admin/account" class="col-sm-12 col-md-12 col-lg-12" method="POST">

                        @csrf

                        <div class="row">

                            <div class="form-group col-sm-12 col-md-12 col-lg-6">
                                <label for="referenceNumber">Bank Name</label>
                                <input type="text" value="{{optional($account)->bank_name}}" name="bank_name" required class="form-control  input-box b-white">
                            </div>
                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="accountName">Account Name</label>
                                <input type="text" value="{{optional($account)->account_name}}" name="account_name" required class="form-control  input-box b-white">
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="transactionDate">Bank Account Number</label>
                                <input type="text" name="account_number" value="{{optional($account)->account_number}}" required class="form-control  input-box b-white">
                            </div>
                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="paymentDate">Bank Sort Code</label>
                                <input type="text" name="sort_code" value="{{optional($account)->sort_code}}" required class="form-control  input-box b-white">
                            </div>

                        </div>

                        <div class="row">
                            <div class="form-group col-12">
                                <button type="submit" class="btn btn-blue">Update Account Details</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

        </div>



    </div>
</div>

@endsection