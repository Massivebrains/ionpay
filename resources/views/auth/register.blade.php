@extends('layouts.site')

@section('content')
<section class="section sect section-concept section-no-border section-dark  pt-5 m-0" id="section-concept5" >

	<div class="container first-container">

		<div class="row mb-5 counters contact-us">

			<div class="col-lg-5 border-right pr-5">
				<h2 class="font-weight-bold text-9 text-white mb-0 appear-animation contact-h"  data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}">Create Account</h2>
				<p class="text-5  negative-ls-05 pt-3 pb-4 mb-5 appear-animation text-white"  style="color: #fff !important;" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750">Sign Up as a Merchant.</p>
			</div>
			<div class="col-lg-6 offset-lg-1 ">
				<form>
					<div class="row">

						<div class="form-group col-lg-12">
							<input type="text" class="form-control customise  pull-left mb-3 " style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" placeholder="Name">
						</div>
						<div class="form-group col-lg-6">
							<input type="text" class="form-control customise  pull-left mb-3 mr-5" style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" placeholder="Email">
						</div>
						<div class="form-group col-lg-6">
							<input type="text" class="form-control customise  pull-left mb-3" style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" placeholder="Phone Number">
						</div>
						<div class="form-group col-lg-6">
							<input type="password" class="form-control customise  pull-left mb-3 "  style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" id="password" name="password" placeholder="Create Password">
							<i class="fa fa-eye pull-right" style="font-size: 16px"></i>
						</div>
						<div class="form-group col-lg-6">
							<input type="password" class="form-control customise  pull-left mb-3" style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" id="cpassword" name="password" placeholder="Confirm Password">
							<i class="fa fa-eye-slash pull-right" style="font-size: 16px"></i>
						</div>

						<button class="btn btn-danger wide-btn">Submit</button>

					</div>
				</form>
			</p>
		</div>
	</div>
</div>
</section>
<section class="section  section-concept sect section-no-border section-dark section-angled section-angled-reverse pt-5 m-0" >


	<div class="container text-center">
		<img alt="Porto" class="screen" width="auto" height="300" data-sticky-width="82" data-sticky-height="36" data-sticky-top="0" src="img/controlv2/screens_sign_up.png"/>


	</div>

</section>


@endsection