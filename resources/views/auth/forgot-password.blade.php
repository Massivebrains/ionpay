@extends('layouts.site')

@section('content')

<section class="section sect section-concept section-no-border section-dark  pt-5 m-0" id="section-concept5" >

	<div class="container first-container">

		<div class="row mb-5 counters contact-us">

			<div class="col-lg-5 border-right pr-5">
				<h2 class="font-weight-bold text-9 text-white mb-0 appear-animation contact-h"  data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}">Forgot Pasword</h2>
				<p class="text-5  negative-ls-05 pt-3 pb-4 mb-5 appear-animation text-white"  style="color: #fff !important;" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750">Please provide your Merchant email address.</p>
			</div>
			<div class="col-lg-6 offset-lg-1 ">
				<form method="POST" action="/forgot-password">

					@csrf

					@include('components.alert')

					<div class="row">

						<div class="form-group col-lg-12">
							<input name="email" required type="emial" class="form-control customise  pull-left mb-3  mt-5" style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" placeholder="Email Address">
						</div>
						
						<button class="btn btn-danger wide-btn" type="submit">Get reset Link</button>
					</div>
				</form>
			</p>
		</div>
	</div>
</div>
</section>

<section class="section  section-concept sect section-no-border section-dark section-angled section-angled-reverse pt-5 m-0" >


	<div class="container text-center">
		<img class="screen" alt="Porto" width="auto" height="300" data-sticky-width="82" data-sticky-height="36" data-sticky-top="0" src="/img/controlv2/screens_sign_up.png"/>
	</div>

</section>
@endsection