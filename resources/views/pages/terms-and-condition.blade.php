@extends('layouts.site')

@section('content')

<section class="section  section-concept sect section-no-border section-dark section-angled section-angled-reverse pt-5 m-0" >


	<div class="container first-container">

		<div class="row pb-5">
			<div class="col-lg-12 ">
				<h1 class="font-weight-bold text-center text-9 text-color-dark mb-5 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms;">Terms and Condition</h1>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">Introduction
                </h1>
                <p>
                    These Website Terms and Conditions of Use (“Terms”) contained herein on this webpage is a legal agreement between you, as a prospective customer of I-ONE-C Limited (I-ONE-C, “we”, “our” or “us”) and shall govern your access to and use of the services on our online payment platform hereinafter referred to as “GetIOnePay” which include all pages within the website, mobile applications and other products and services (collectively referred to as the “Services”).
                </p>
                <p>These Terms apply in full force and effect to your use of the Services and by using any of the Services, you expressly accept all terms and conditions contained herein in full and without limitation or qualification, including our Privacy Policy. You must not use any of the Services, if you have any objection to any of these Terms.</p>
                <p>PLEASE READ AND UNDERSTAND THE TERMS OF AGREEMENT CAREFULLY BEFORE AGREEING TO BE BOUND BY ITS TERMS.</p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Governing Language
                </h1>
                <p>
                    The governing language of these terms and all communication between you and us will be English language.
                </p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Who May Use Our Services?
                </h1>
                <p>
                    You may use the Services only if you agree to form a binding contract with us and are not a person barred from receiving services under the laws of the applicable jurisdiction. If you are accepting these Terms and using the Services on behalf of a company, business, or organization, you represent and warrant that you are authorized to do so.
                </p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Age Restriction
                </h1>
                <p>
                    Our website and services are directed to people from the ages of 18 and above. You affirm that you are 18 years or of such legal age as may be permissible in your jurisdiction to enter a legally binding contract. We do not knowingly engage people younger than the age of 18.
                </p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Intellectual Property
                </h1>
                <p>
                    Unless otherwise indicated or anything contained to the contrary or any proprietary material owned by a third party and so expressly mentioned, we own all Intellectual Property Rights to and into GetIOnePay, including, without limitation, any and all rights, title and interest in and to copyright, related rights, patents, utility models, trademarks, trade names, service marks, designs, know-how, trade secrets and inventions (whether patentable or not), goodwill, source code, meta tags, databases, text, content, graphics, icons, and hyperlinks. You acknowledge and agree that you shall not use, reproduce or distribute any content from GetIOnePay belonging to I-ONE-C without obtaining authorization from us.
                </p>
                <p>
                    We respect your right to ownership of content created or stored by you. Unless specifically permitted by you, your use of the service(s) does not grant us the license to use, reproduce, adapt, modify, publish or distribute the content created by you or stored in your user account for our commercial, marketing or any similar purpose. But you grant us permission to access, copy, distribute, store, transmit, reformat, publicly display and publicly perform the content of your user account solely as required for the purpose of providing the Services to you.</p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Data Protection
                </h1>
                <p>
                    We recognize the importance of protecting the privacy of those who visit and choose to use GetIOnePay. I-ONE-C remains compliant with the Payment Card Industry Data Security Standard requirement, which may be amended from time to time. The Personal information you provide to I-ONE-C through the Service are at all times compliant with all data protection laws in the territory and all applicable data protection regulations in force. We also maintain a Privacy Policy which provides an overview of the Personal Information we collect about you. By using GetIOnePay, you consent to such processing and commit to provide accurate information. Your election to use the service indicates your acceptance of the terms of the GetIOnePay Privacy Policy. You are responsible for maintaining confidentiality of your username, password and other sensitive information. You are also responsible for all activities that occur in your user account and you agree to inform us immediately of any unauthorized use of your user account by email or by calling us on any of the numbers listed on the website.  Do not share, or make public, any of your sensitive account and login information as we shall not be responsible for any loss or damage to you or to any third party incurred as a result of any unauthorized access and/or use of your user account, or otherwise.
                </p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    License to Use Our Website
                </h1>
                <p>
                    We grant you a non-assignable, non-exclusive and revocable license to use the software provided as part of our services in the manner permitted by these Terms. This license grant includes all updates, upgrades, new versions and replacement software for you to use in connection with our services.
                </p>
                <p>The services are protected by copyright, trademark, and other laws of both Nigeria and foreign countries. Nothing in this Term gives you a right to use our name or any of our trademarks, logos, domain names, and other distinctive brand features. All right, title and interest in and to the services are and will remain the exclusive property of I-ONE-C and its licensors.</p>
                <p>If you do not comply with all the provisions, then you will be liable for all resulting damages suffered by you, I-ONE-C and all third parties. Unless otherwise provided by applicable law, you agree not to alter, re-design, reproduce, adapt, display, distribute, translate, disassemble, reverse engineer, or otherwise attempt to create any source code that is derived from the software.</p>
                <p>Any feedback, comments, or suggestions you may provide to us and our services is entirely voluntary and we will be free to use such feedback, comments or suggestion as we see fit without any obligation to you.</p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Communications from I-ONE-C
                </h1>
                <p>The Service may include certain communications from I-ONE-C, such as service announcements, administrative messages and newsletters. You understand that these communications shall be considered part of using GetIOnePay. As part of our policy to provide you total privacy, we also provide you the option of opting out from receiving newsletters from us. However, you will not be able to opt-out from receiving service announcements and administrative messages.</p>
                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Restriction of Use
                </h1>
                <p>In addition to all other terms and conditions of this Terms of Use, you shall not:
                    (i) transfer or otherwise make available to any third party the Services; (ii) provide any service based on the Services without prior written permission; (iii) use the third party links to sites without agreeing to their website terms & conditions; (iv) post links to third party sites or use their logo, company name, etc. without their prior written permission; or (v) use the Services for spamming and other illegal purposes.
                </p>
                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Cancellation
                </h1>
                <p>You have the right to cancel your account at any time. Once the account is cancelled all information associated with that account will be permanently deleted. You are solely responsible for account cancellation.
                </p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Information Security and Warranty Disclaimer
                </h1>
                <p>I-ONE-C WILL ALWAYS ENSURE THAT THE WEBSITE IS AVAILABLE AT ALL TIMES AND BUG FREE. HOWEVER, IT IS USED AT YOUR OWN RISK. WE PROVIDE ALL MATERIALS “AS IS” WITH NO WARRANTY, EXPRESS OR IMPLIED, OF ANY KIND. WE EXPRESSLY DISCLAIM ANY AND ALL WARRANTIES AND CONDITIONS, INCLUDING ANY IMPLIED WARRANTY OR CONDITION OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AVAILABILITY, SECURITY, TITLE, AND NON-INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS, WITHOUT LIMITING THE GENERALITY OF THE FOREGOING.
                </p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Limitation of Liability
                </h1>
                <p>YOU AGREE THAT I-ONE-C SHALL, IN NO EVENT, BE LIABLE FOR ANY CONSEQUENTIAL, INCIDENTAL, DIRECT INDIRECT, SPECIAL, PUNITIVE, OR OTHER LOSS OR DAMAGE WHATSOEVER OR FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, COMPUTER FAILURE, LOSS OF BUSINESS INFORMATION, OR OTHER LOSS ARISING OUT OF OR CAUSED BY YOUR USE OF OR INABILITY TO USE THE SERVICE, EVEN IF I-ONE-C HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. IN NO EVENT SHALL I-ONE-C’S ENTIRE LIABILITY TO YOU IN RESPECT OF ANY SERVICE, WHETHER DIRECT OR INDIRECT, EXCEED THE FEES PAID BY YOU TOWARDS SUCH SERVICE.

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Indemnification
                </h1>
                <p>To the extent permissible under law, You agree to indemnify, defend and hold harmless I-ONE-C its officers, directors, employees, suppliers, and affiliates, from and against any and all losses, liabilities, claims, damages, costs and expenses (including legal fees and disbursements in connection therewith and interest chargeable thereon) asserted against or incurred that arise out of, result from, or may be payable by virtue of, any breach or non-performance of any representation, or claims that you have used the services in violation of another party’s right, in violation of any law, in violation of any provisions of the Terms, or any other claim related to your use GetIOnePay.
                </p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Breach of Terms
                </h1>
                <p>
                    Without prejudice to I-ONE-C’s other rights under this Terms, if you breach these terms in any way, we may take such action as we deem fit to deal with the breach, including suspending your access to the website, app, blocking computers using your IP address from accessing the site , contacting your internet service provider to request that they block your access to the site and/or instituting an action again you in a court of law.
                </p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Modification of Terms of Use
                </h1>
                <p>
                    We may modify these Terms upon notice to you at any time. These modifications may include, without limitation, payment for the Services. Ensure that you check the Terms of Use from time to time for available modification because your continued use of this platform after modification will be deemed to be your agreement to the amended Terms.
                </p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Termination
                </h1>
                <p>
                    In addition to any other legal or equitable remedies, we may, without prior notice to you, immediately terminate the Terms and Conditions of Use or revoke any or all of your rights granted under the Terms and Conditions of Use.
                </p>
                <p>Upon any termination of this Agreement, you shall immediately cease all access to and use of the Site and we shall, in addition to any other legal or equitable remedies, immediately revoke all password(s) and account identification issued to you and deny your access to and use of this Site in whole or in part. Any termination of this agreement shall not affect the respective rights and obligations (including without limitation, payment obligations) of the parties arising before the date of termination. You furthermore agree that the Site shall not be liable to you or to any other person as a result of any such suspension or termination.</p>
                <p>If you are dissatisfied with the Site or with any terms, conditions, rules, policies, guidelines, or practices of I-ONE-C in operating the Site, your sole and exclusive remedy is to discontinue using the Site.</p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Governing Law
                </h1>
                <p>This Agreement shall be interpreted, enforced and construed in accordance with and governed by the Laws of the Federal Republic of Nigeria without reference to conflict of laws principles, and disputes arising in relation hereto shall be subject to the exclusive jurisdiction of the courts in Nigeria.

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Severability
                </h1>
                <p>If any portion of these terms or conditions is held by any court or tribunal to be invalid or unenforceable, either in whole or in part, then that part shall be severed from these Terms and Conditions of Use and shall not affect the validity or enforceability of any other section listed in this document.

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    General Provisions
                </h1>
                <p>You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing. Assigning or sub-contracting any of your rights or obligations under these Terms and Conditions of Use to any third party is prohibited unless agreed upon in writing by the seller.
                    We reserve the right to transfer, assign or sub-contract the benefit of the whole or part of any rights or obligations under these Terms of Use to any third party.

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Complaints
                </h1>
                <p>If you have any questions or concerns regarding this Terms of Service, do not hesitate to send same to hello@GetIOnePay.com</p>

            </div>
		</div>
	</div>

</section>
@endsection
