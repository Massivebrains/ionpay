@extends('layouts.site')

@section('content')

<section class="section  section-concept sect section-no-border section-dark section-angled section-angled-reverse pt-5 m-0" >


	<div class="container first-container">

		<div class="row pb-5">
			<div class="col-lg-4 text-center offset-lg-4">
				<h1 class="font-weight-bold text-9 text-color-dark mb-5 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms;">Careers</h1>
				<h1 class="font-weight-bold text-5 text-color-dark mb-5 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1.5;">We are currently not recruiting. <br>Please follow our <a href="https://www.instagram.com/getionepay/" style="color: #00AEEF">Instagram</a> page to stay updated on upcoming career opportunities.</h1>


				<img alt="Porto" width="auto" height="auto" data-sticky-width="82" data-sticky-height="36" data-sticky-top="0" class="chair" src="/img/controlv2/career-chair.svg"/>


			</div>
		</div>
	</div>

</section>
@endsection
