@extends('layouts.site')

@section('content')
<section class="section sect section-concept section-no-border section-dark  pt-5 m-0" id="section-concept5" >


	<div class="container first-container">

		<div class="row mb-5 counters contact-us">

			<div class="col-lg-5 border-right pr-5">
				<h2 class="font-weight-bold text-9 text-white mb-0 appear-animation contact-h"  data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}">Contact Us</h2>
				<p class="text-5  negative-ls-05 pt-3 pb-4 mb-5 appear-animation text-white"  style="color: #fff !important;" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750">Need to make an inquiry? Or have issues or complaints? Please fill the form below and a representative will attend to you.</p>
			</div>
			<div class="col-lg-6 offset-lg-1 ">
				<form>
					<div class="row">
						<div class="form-group col-lg-12 mt-5">
							<input type="text" class="form-control customise  pull-left mb-3 " style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" placeholder="Name">
						</div>
						<div class="form-group col-lg-6">
							<input type="text" class="form-control customise  pull-left mb-3 mr-5" style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" placeholder="Email">
						</div>
						<div class="form-group col-lg-6">
							<input type="text" class="form-control customise  pull-left mb-3" style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" placeholder="Phone Number">
						</div>

						<div class="form-group col-lg-12">
							<textarea type="text" class="form-control customise pull-left mb-3" style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" placeholder="Your Message"></textarea>
						</div>
						<button class="btn btn-danger wide-btn wide-btn-b">Submit</button>

					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<section class="section  section-concept sect section-no-border section-dark section-angled section-angled-reverse pt-5 m-0" id="faq">


	<div class="container">

		<div class="row pb-5">
			<div class="col-lg-8 text-center offset-lg-2">
				<h1 class="font-weight-bold text-9 text-color-dark mb-5 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms;">Frequently Asked <br>Questions</h1>

				<div class="toggle toggle-primary" data-plugin-toggle>
					<section class="toggle active">
						<label class="text-4">What products can I sell with iOnePay?</label>
						<p class="text-color-dark">Merchants can sell any type of products with Getionepay. Like clothing, housewares, electricals, books, groceries etc. You can also sell event tickets, or invite people to donate to a cause. Simply generate your unique product code and display it to your customers.
						</p>
					</section>

					<section class="toggle">
						<label>How do I register?</label>
                        <p class="text-color-dark" style=" height: 0px;">Text Merchant to 09061600066, you’d get an invitation code sent to you. Input your invitation code and login to access your merchant dashboard.</p>
                    </section>

					<section class="toggle">
						<label>How do I generate unique product codes?</label>
                        <p class="text-color-dark" style="height: 0px;">Log in to your dashboard using your merchant invitation code to create your products, events etc. Once done, the platform generates a unique code for the product. </p>
                    </section>

					<section class="toggle">
						<label>How do my customers shop with product codes? </label>
                        <p class="text-color-dark" style="height: 0px;">When you send product code to customers, they can shop by sending the code to any of our channels and the selected platform will guide them through the steps to completing their order.</p>
                    </section>

				</div>
			</div>
		</div>
	</div>

</section>
<section class="section sect section-concept sect section-no-border section-dark section-angled section-angled-reverse pt-5 m-0" id="section-concept">
	<div class="container pt-5 mt-5 ">

	</div>
</section>

@endsection
