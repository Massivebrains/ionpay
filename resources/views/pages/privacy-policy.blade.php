@extends('layouts.site')

@section('content')

<section class="section  section-concept sect section-no-border section-dark section-angled section-angled-reverse pt-5 m-0" >


	<div class="container first-container">

		<div class="row pb-5">
			<div class="col-lg-12 ">
				<h1 class="font-weight-bold text-center text-9 text-color-dark mb-5 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms;">Privacy Policy</h1>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">Overview
                </h1>
                <p>
                    We respect the privacy of everyone who visits our website, payment platforms (“Platforms”), Software Applications (“Apps”) and want to protect your personal information. As a result, we would like to inform you on the way we would use your personal data. We recommend you to please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your personal data as well as the choices available to you regarding how you can access and update this information.
                </p>
                <p>By submitting your personal data to us, you will be treated as having given your permission- where necessary and appropriate- for disclosures referred to in this policy.</p>
                <p>The term "us" or "we" or "our" refers to I-ONE-C Limited, the owner of GetIOnePay.</p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">Definition
                </h1>
                <p><b>Cookies - </b>means	a small data file that is transferred to your computer or mobile device. It enables us to remember your account log-in information, IP addresses, web traffic number of times you visit, browser type and version, device details, date and time of visits;</p>
                <p><b>I-ONE-C  - </b>means I-ONE-C Limited and subsidiaries or affiliates;</p>
                <p><b>Personal Information   - </b>means any information that can be used to identify a living person including email address, company name, password, payment card, financial information such as bank account number, etc.), Government-issued Identity card, Bank Verification Number (BVN) and/or taxpayer identification it may also include anonymous information that is linked to you, for example, your internet protocol (IP), log-in information, address, location, device or transaction data;</p>
                <p><b>Sites  - </b>means any platform including but not limited to mobile applications, websites and social media platforms;</p>
                <p><b>User  - </b>means an individual who uses the Services or accesses the Sites and has agreed to use the end services;</p>
                <p><b>Special Categories of
                        Personal Information   - </b>means details about your race or ethnicity, religious or philosophical beliefs, sex life, sexual orientation, political opinions, trade union membership, information about your health and genetic and biometric data.</p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">Objective
                </h1>
                <p>This privacy policy statement is to provide all persons and organizations whose personal data we hold with description of the types of Personal Information we collect, the purposes for which we collect that Personal Information, the other parties with whom we may share it and the measures we take to protect the security of the data.</p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">Our Privacy Principles
                </h1>
                <p>I-ONE-C focuses on the following core principles
                <ul>
                    <li>To Empower the individual: We want you to be in charge of your personal information and to make your own voluntary choices about your personal data;</li>
                    <li>To keep and secure personal information: We do not take your trusting us with your information for granted. We take responsibility to ensuring that appropriate security measures are put in place and your personal information is protected;</li>
                    <li>To be transparent and to educate users: For you to know what personal information is, how we collect personal information, for what purpose and how we secure personal information;</li>
                    <li>To collect and store personal information on the “need to collect” basis: We collects personal information to perform its services for you. We work to have measures in place to prevent collecting and storing personal data beyond what we need.</li>

                </ul>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">Personal Information We May Collect About You
                </h1>
                <p>We may collect, use, process, store, or transfer personal information such as:
                <ul>
                    <li>Identity Data: Information such as, your full name(s), your government-issued identity number, bank verification number (BVN) and your date of birth. This data is to enable us to verify your identity in order to offer our services to you;</li>
                    <li>Contact Data: This is data that is needed to reach out to you, such as your contact address, email address, telephone number, details of the device you use and billing details;</li>
                    <li>Identification documents :(such as your passport or any Government-issued identity card), a photograph (if applicable) and any other registration information you may provide to prove you are eligible to use our services;</li>
                    <li>Log/Technical information: When you access GetIOnePay, our servers automatically record information that your browser sends whenever you visit a website, links you have clicked on, length of visit on certain pages, unique device identifier, log-in information, location and other device details.</li>
                   <li>Financial Data: Information, such as personal account number, the merchant’s name and location, the date and the total amount of transaction, and other information provided by financial institutions or merchants when we act on their behalf;</li>
                   <li>Transactional Data: These are information relating to a payment when you as a merchant (using one or more of our payment processing services) or as a customer, are using our products or services;</li>
                    <li>Marketing and Communications Data: This includes both a record of your decision to subscribe or to withdraw from receiving marketing materials from us or from our third parties.</li>
                    <li>Records of your discussions with us, if we contact you and if you contact us.</li>
                </ul>

                <p>We may also collect, store, use and transfer non-personal information such as statistical or demographic data.</p>

                <p>As a principle, we do not collect any Special Categories of Personal Information. If we do collect Special Categories of Personal Information, we will ensure compliance with applicable law.</p>
                <p>This Privacy Policy applies to GetIOnePay only. We do not exercise control over the sites displayed or linked from within our various services. These other sites may place their own cookies, plug-ins or other files on your computer, collect data or solicit personal information from you. I-ONE-C does not control these third-party websites and we are not responsible for their privacy statements. Please consult such third parties’ privacy statements.</p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    How we get your Personal Information and why we have it
                </h1>
                <p>The Personal Information we have about you is directly made available to us when you:
                <ul>
                    <li>Sign up for an GetIOnePay Account;</li>
                    <li>Use any of our services;</li>
                    <li>Contact our customer support team;</li>
                    <li>Fill our online forms;</li>
                    <li>Contact us;</li>
                </ul>

                <p>The lawful basis we rely on for processing your Personal Information are:
                <ul>
                    <li>Your Consent: Where you agree to us collecting your Personal Information by using our Services.</li>
                    <li>We have a contractual obligation: Without your Personal Information, we cannot provide our Services to you.</li>
                    <li>We have a legal obligation: To ensure we are fully compliant with all applicable financial legislations such as Anti-Money Laundering and Counter Terrorist Financing Laws, we must collect and store your Personal Information. We protect against fraud by checking your identity with your Personal Information.</li>
                    <li></li>
                </ul>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    How We May Use Your Personal Information
                </h1>
                <p>We may use your Personal Information we collect to:
                <ul>
                    <li>Create and manage any accounts you may have with us, verify your identity, provide our services, and respond to your inquiries;</li>
                    <li>Process your payment transactions (including authorization, clearing, chargebacks and other related dispute resolution activities);</li>
                    <li>Protect against and prevent fraud, unauthorized transactions, claims and other liabilities;</li>
                    <li>Provide, administer and communicate with you about products, services, offers, programs and our promotions, financial institutions, merchants and partners;</li>
                    <li>Evaluate and improve our business, including developing new products and services;</li>
                    <li>To target advertisements, newsletter and service updates;</li>
                    <li>As necessary to establish, exercise and defend legal rights;</li>
                    <li>As may be required by applicable laws and regulations, including for compliance with Know Your Customers and risk assessment, Anti-Money Laundering, anti-corruption and sanctions screening requirements, or as requested by any judicial process, law enforcement or governmental agency having or claiming jurisdiction over I-ONE-C or our affiliates;</li>
                    <li>To use data analytics to improve our Site, products or services, and user experiences.</li>
                    <li>For other purposes for which we provide specific notice at the time of collection.</li>
                </ul>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Data Security and Retention
                </h1>
                <p>We maintain administrative, technical and physical controls designed to protect the Personal Information you provide, or we collect against loss or theft, as well as against any unauthorized access, risk of loss, disclosure, copying, misuse or modification.
                <p>Other security measures include but not limited to, secure servers, firewall, data encryption and granting access only to employees in order to fulfil their job responsibilities.</p>
                <p>Where you use a password for any of your accounts, please ensure you do not share this with anyone, and the password is kept confidential at all times.</p>
                <p>We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of your Personal Information is protected and maintained. Transmitting information online is not entirely secure. As such, we cannot guarantee that all information provided online is secure. We would take all reasonable steps to ensure that your Personal Information is secured and well protected.</p>
                <p>We will only retain personal information on our servers for as long as is reasonably necessary as long as we are providing Services to you. Where you close your Account, your information is stored on our servers to the extent necessary to comply with regulatory obligations and for the purpose of fraud monitoring, detection and prevention. Where we retain your Personal Data, we do so in compliance with limitation periods under the applicable law.</p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Disclosing Your Personal Information
                </h1>
                <p>We may disclose or share your Personal Information with third parties which include our affiliates, employees, officers, service providers, agents, suppliers, subcontractors as may be reasonably necessary for the purposes set out in this policy.</p>
                <p>I-ONE-C only shares personal information with External Third parties in the following limited circumstances:</p>

                <ul>
                    <li>We have your consent. We require opt-in consent for the sharing of any sensitive personal information;</li>
                    <li>We provide such information to our subsidiaries, affiliated companies or other trusted businesses or persons for the purpose of processing personal information on our behalf. We require that these parties agree to process such information based on our instructions and incompliance with this Privacy Policy and any other appropriate confidentiality and security measures</li>
                    <li>We have a good faith belief that access, use, preservation or disclosure of such information is reasonably necessary to:
                    <ul>
                        <li>satisfy any applicable law, regulation, legal process or enforceable governmental request,</li>
                        <li>enforce applicable Terms of Service, including investigation of potential violations thereof</li>
                        <li>detect, prevent, or otherwise address fraud, security or technical issues, or</li>
                        <li>protect against imminent harm to the rights, property or safety of I-ONE-C, its users or the public as required or permitted by law</li>
                    </ul>
                    </li>
                    <li>If I-ONE-C becomes involved in a merger, acquisition, or any form of sale of some or all of its assets, we will provide notice before personal information is transferred and becomes subject to a different privacy policy.</li>
                </ul>
                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Marketing
                </h1>
                <p>We may process your Personal Information in order to contact you or send you marketing content and communication about our products, services or surveys. You may exercise your right to object to such contact from us or opt out from the marketing content. Please note however that if you opt-out of marketing content, we may still send you messages relating to transactions and our Services related to our ongoing business relationship.
                <p>We may ask you for permission to send notifications to you. Our Services will still work if you do not grant us consent to send you notifications.</p>

                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Your Data Protection Rights and Choices
                </h1>
                <p>Based on your location and applicable laws, below are the rights you have as a user in relation to your Personal Information:</p>
                <ul>
                    <li>Right to be informed</li>
                    <li>Right to request access or copies to your Personal Information by signing into your account or contacting us</li>
                    <li>Right to request that we delete your Personal Information. You have the right to ask us to erase your Personal Information. Please note that this is a limited right which applies where the data is no longer required, or the processing has no legal justification. The exceptions to this right is where the applicable law requires us to retain a historical archive or where we retain a core set of your personal data to ensure we do not inadvertently contact you in future where you object to your data being used for marketing purposes.</li>
                    <li>Right to correct or rectify any Personal Information that you provide that may be false, out of date or inaccurate. You also have the right to ask us to complete information you think is incomplete.</li>
                    <li>Right to object to the processing of your Personal Information for marketing purposes. You have a right to ask us not to contact you for marketing purposes by adjusting your notification preference on the settings page or by opting out via the unsubscribe link in marketing emails we send you</li>
                    <li>Right to object to processing: You have the right to object to the processing of your Personal Information in certain circumstances. Please note that where you object to us processing your Personal Information, we might be unable to provide the services to you.</li>
                    <li>To target advertisements, newsletter and service updates;</li>
                    <li>As necessary to establish, exercise and defend legal rights;</li>
                    <li>As may be required by applicable laws and regulations, including for compliance with Know Your Customers and risk assessment, Anti-Money Laundering, anti-corruption and sanctions screening requirements, or as requested by any judicial process, law enforcement or governmental agency having or claiming jurisdiction over us or our  affiliates;</li>
                    <li>To use data analytics to improve our Website, products or services, and user experiences.</li>
                    <li>For other purposes for which we provide specific notice at the time of collection.</li>
                </ul>
                <p>The basis of we processing your Personal Information is your consent. You also have the choice at any time to withdraw consent which you have provided.</p>
                <p>You also have a choice to deactivate your account at any time. You may contact us should you wish to de-activate your Account at any time via the Contact Us segment below or send us an email at privacy@GetIOnePay.com.</p>
                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Cookies
                </h1>
                <p>Like many other websites, we use cookies to distinguish you from other users and to customize and improve our services.</p>
                <p>Some browsers may automatically accept cookies while some can be modified to decline cookies or alert you when a website wants to place a cookie on your computer. If you do choose to disable cookies, it may limit your ability to use our website. For detailed information on the cookies and how we use them see our Cookie Policy</p>
               <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                   Minor
                </h1>
                <p>I-ONE-C’s websites and applications are not directed at persons under the age of eighteen (18) and we do not collect any Personal Information knowingly or directly from minors who fall within this category.</p>
                <p>Where you have any belief that I-ONE-C has mistakenly or unknowingly collected information from a minor, please <a href="/connect">contact us</a> using the information provided under the Contact Us section to enable investigate and restrict such data.</p>
                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    International Data Transfers
                </h1>
                <p>Our business is global with affiliates and service providers located around the world. As such, Personal Information may be stored and processed in any country where we have operations. Also, your Personal Data may be transferred to countries which may not have the same data protection laws as the country you initially provided your Personal Information, but whenever we transfer or transmit your Personal Information internationally, we will ensure or take reasonable steps to ensure your Personal Information is handled securely in line with the applicable data protection laws and standard contractual clauses.</p>
                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Updates to our privacy policy
                </h1>
                <p>From time to time, we may change, amend or review this Privacy Policy from time to time to reflect new services or changes in our Privacy Policy and place any updates on this page. All changes made will be posted on this page and where changes will materially affect you, we will notify you of this change by placing a notice online or via mail. If you keep using our Services, you consent to all amendments of this Privacy Policy.</p>
                <h1 class="font-weight-bold text-5 text-color-dark mb-2 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms; line-height: 1;">
                    Contact Us
                </h1>
                <p>All access requests, questions, comments, complaints and other requests regarding the privacy policy should be sent to privacy@GetIOnePay.com.</p>
                <p>We may request additional details from you regarding your complaints and keep records of your requests and resolution.</p>


                <p></p>
                <p></p>


            </div>
		</div>
	</div>

</section>
@endsection
