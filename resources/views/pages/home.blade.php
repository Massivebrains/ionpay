@extends('layouts.site')

@section('content')

<section class="section section-concept section-no-border section-dark section-angled section-angled-reverse pt-5 m-0 cover" id="section-concept2">

	<div class="container pt-5 mt-5">
		<div class="row align-items-center pt-3">
			<div class="col-lg-5 mb-lg-5">
				<h1 class="font-weight-bold text-12 text-dark line-height-2 mb-lg-3 appear-animation animated fadeInUpShorter appear-animation-visible brand block-pc-hide" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" data-appear-animation-duration="750" style="animation-delay: 400ms;">
					A brand new wave for <br>E-commerce!
				</h1>
				<h1 class="font-weight-bold text-12 text-dark line-height-2 mb-lg-3 appear-animation animated fadeInUpShorter appear-animation-visible brand btn-mobile-hide" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" data-appear-animation-duration="750" style="animation-delay: 400ms;">
					A brand new wave for E-commerce!
				</h1>
				<p class="custom-font-size-1 text-5 appear-animation text-dark animated fadeInUpShorter appear-animation-visible brand-p" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="900" data-appear-animation-duration="750" style="margin-bottom: 90px;animation-delay: 900ms;">
					Provide a seamless shopping experience for your customers and increase shopability.
				</p>

			</div>
			<div class="col-lg-6 offset-lg-1 mb-5 appear-animation animated fadeIn appear-animation-visible" data-appear-animation="fadeIn" data-appear-animation-delay="1200" data-appear-animation-duration="750" style="animation-delay: 300ms;">
				<img src="/img/controlv2/home_man.svg" class="img-fluid  d-none d-lg-block appear-animation animated fadeInUp appear-animation-visible" alt="Ionepay Man" data-appear-animation="fadeInUp" data-appear-animation-delay="600" style="animation-delay: 600ms;">

			</div>

		</div>
	</div>
</section>

<section class="section sect section-concept sect section-no-border section-dark section-angled section-angled-reverse pt-5" id="section-concept">


	<div class="container">
		<div class="row counters">
			<div class="col-sm-8 col-lg-6 counter mb-md-0 beside-about">
				<img src="/img/controlv2/about_bag.svg"  class="img-fluid imgsvg d-none d-lg-block appear-animation animated fadeInUp appear-animation-visible" alt="Screenshot 2" data-appear-animation="fadeInUp" data-appear-animation-delay="600" style="animation-delay: 600ms;">
			</div>
			<div class="col-sm-12 col-lg-6 counter mb-5 mb-md-0 about">
				<h1 class="font-weight-bold text-9 mb-0 appear-animation animated appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}">About</h1>

				<div class="appear-animation  board-about" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="750" data-appear-animation-duration="750">

					<p class="text-1rem text-color-default negative-ls-05 pt-3  appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750">
						Why limit your customers to just one payment option? iOnePay is a secure payment platform that does not require an internet connection to perform transactions.
						<br><br>
						Our platform allows vendors to receive payments from customers via different channels including SMS, Whatsapp, Facebook, Telegram, Twitter, Skype, Slack and other social media platforms. Shoppers do not need internet access to make payments anymore.
						<br><br>
						Simply input the unique product code and choose one of our channels to make your payment. No credit or debit card required.
					</p>
				</div>
				<div class="show-after"></div>
			</div>
		</div>
	</div>
	<div class="container" id="how-it-works">
		<div class="row counters">

			<div class="col-sm-12 col-lg-7 counter mb-md-0 how">
				<h1 class="font-weight-bold text-9 mb-0 appear-animation animated appear-animation-visible how-title" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}">How to Use</h1>

				<div class="appear-animation board" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="750" data-appear-animation-duration="750">

					<p class="text-1rem text-color-default negative-ls-05 pt-3 pb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750">
						iOnePay generates unique codes for vendors and the different products they sell. These unique codes are then used to create order IDs that enable customers to shop the products attached to them.
						<br><br>
						Customers simply input the codes into the messaging platform, and an automatic response guides them through the buying process.


					</p>
				</div>
			</div>
			<div class="col-sm-8 col-lg-5 counter mb-md-0 beside-how">
				<img src="/img/controlv2/how_phone.svg"  class="img-fluid imgsvg  appear-animation animated fadeInUp appear-animation-visible" alt="Screenshot 2" data-appear-animation="fadeInUp" data-appear-animation-delay="600" style="animation-delay: 600ms;">
			</div>
		</div>
	</div>

</section>

<section class="section sect section-concept sect section-no-border  paywithOrderid pt-5 m-0" id="section-concept3" >


	<div class="container pb-5 pt-5">

		<div class="row mb-5 pb-lg-3 counters ">

{{--			<div class="col-lg-4 ">--}}
{{--				<h2 class="font-weight-bold text-9 text-white mb-0 appear-animation" style="color: #fff !important;" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}">Pay with Order ID</h2>--}}
{{--				<p class="text-5  negative-ls-05 pt-3 pb-lg-4 mb-lg-5 appear-animation text-white"  style="color: #fff !important;" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750">Input your Order ID below or select one of our channels to make your payment.</p>--}}
{{--			</div>--}}
{{--			<div class="col-lg-7 offset-lg-1">--}}
{{--				<p class="text-1rem negative-ls-05 pt-3 appear-animation green-online" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750" style="color: #68D873 !important;">Online Payment--}}
{{--					<form>--}}
{{--						<div class="row">--}}
{{--							<div class="col-lg-12">--}}
{{--								<input type="text" class="form-control customise long pull-left orderid-input" placeholder="Order ID number">--}}
{{--								<button class="btn btn-danger orderid-btn">Confirm Order</button>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--					</form>--}}
{{--				</p>--}}
{{--			</div>--}}
		</div>
	</div>
</section>


<section class="section sect section-concept sect section-no-border section-dark pt-5 m-0" id="our-channels" >

	<div class="container">
		<div class="text-center mb-5">
			<h1 class="font-weight-bold text-9 mb-0 appear-animation animated appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}">Our Channels</h1>
			<p class="negative-ls-05 pt-3 pb-4 appear-animation text-5 text-white" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750">
				Choose any of these channels to start a conversation.
			</p>
		</div>
		<div class="row pb-5">
			<div class="d-flex col-sm-6 col-lg-3 mb-4  mt-30  appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800" data-appear-animation-duration="750">
				<div class="bg-color-light p-4  border-radius-15">
					<i class="icon icon-sms-channel mb-4"></i>
					<h5 class="text-5" style="text-transform: capitalize">Text Message</h5>
					<p class="text-color-default color-inherit mb-5 text-5">Send a text message to 09061600066</p>
{{--					<button class="btn btn-outline-dark" ><span>Try Now</span> <i class="fa fa-arrow-right"></i></button>--}}
				</div>
			</div>

			<div class="d-flex col-sm-6 col-lg-3 mb-4  mt-30  appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800" data-appear-animation-duration="750">
				<div class="bg-color-light p-4  border-radius-15">
					<i class="icon icon-whatsapp-channel mb-4"></i>
					<h5 class="text-5" style="text-transform: capitalize">Whatsapp</h5>
					<p class="text-color-default color-inherit mb-5 text-5">Send a whatsapp message to this number 09061600066</p>
{{--					<button class="btn btn-outline-dark" ><span>Try Now</span> <i class="fa fa-arrow-right"></i></button>--}}
				</div>
			</div>

			<div class="d-flex col-sm-6 col-lg-3 mb-4  mt-30  appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800" data-appear-animation-duration="750">
				<div class="bg-color-light p-4  border-radius-15">
					<i class="icon icon-twitter-channel icon-feature-8 mb-4"></i>
					<h5 class="text-5" style="text-transform: capitalize">Twitter</h5>
					<p class="text-color-default color-inherit mb-5 text-5">
						Send a Direct Message(DM) to Getionepay and get an instant response
					</p>
					<a href="https://twitter.com/getionepay"><button class="btn btn-outline-dark" ><span>Try Now</span> <i class="fa fa-arrow-right"></i></button></a>
				</div>
			</div>
			<div class="d-flex col-sm-6 col-lg-3 mb-4  mt-30  appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800" data-appear-animation-duration="750">
				<div class="bg-color-light p-4  border-radius-15">
					<i class="icon icon-telegram-channel mb-4"></i>
					<h5 class="text-5" style="text-transform: capitalize">Telegram</h5>
					<p class="text-color-default color-inherit mb-5 text-5">
						Add our contact @ionepayBot to your Telegram Channel
					</p>
					<a href="https://t.me/ionepayBot"><button class="btn btn-outline-dark" ><span>Try Now</span> <i class="fa fa-arrow-right"></i></button>
                    </a>
                </div>
			</div>
			<div class="d-flex col-sm-6 col-lg-3 mb-4  mt-30  appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800" data-appear-animation-duration="750">
				<div class="bg-color-light p-4  border-radius-15">
					<i class="icon icon-facebook-channel mb-4"></i>
					<h5 class="text-5" style="text-transform: capitalize">Facebook</h5>
					<p class="text-color-default color-inherit mb-5 text-5">
						Send us a message on Facebook @Getionpay and get a response instantly
					</p>
					<a href="http://m.me/getionepay"><button class="btn btn-outline-dark" ><span>Try Now</span> <i class="fa fa-arrow-right"></i></button>
                    </a>
                </div>
			</div>
			<div class="d-flex col-sm-6 col-lg-3 mb-4  mt-30  appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800" data-appear-animation-duration="750">
				<div class="bg-color-light p-4  border-radius-15">
					<i class="icon icon-slack-channel mb-4"></i>
					<h5 class="text-5" style="text-transform: capitalize">Slack</h5>
					<p class="text-color-default color-inherit mb-5 text-5">
						Simply add @Getionpay to your workspace
					</p>
					<a href="https://slack.com/oauth/v2/authorize?client_id=14122120950.1214369907824&scope=chat:write,im:history,incoming-webhook,users:write&user_scope=chat:write">
                        <button class="btn btn-outline-dark" ><span>Try Now</span> <i class="fa fa-arrow-right"></i></button>
                    </a>
                </div>
			</div>

			<div class="d-flex col-sm-6 col-lg-3 mb-4  mt-30  appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800" data-appear-animation-duration="750">
				<div class="bg-color-light p-4  border-radius-15">
					<i class="icon icon-skype-channel mb-4"></i>
					<h5 class="text-5" style="text-transform: capitalize">Skype</h5>
					<p class="text-color-default color-inherit mb-5 text-5">
						Add Getionpay to your contact and send us a message on Skype.
					</p>
					<a href="https://join.skype.com/bot/62d60633-68d0-43b8-a1fb-c345c34889ae"><button class="btn btn-outline-dark" ><span>Try Now</span> <i class="fa fa-arrow-right"></i></button>
                    </a>
                </div>
			</div>




		</div>
	</div>

</section>
<section class="section sect section-concept sect section-no-border section-dark  pt-5 m-0" id="section-concept5" >


	<div class="container pb-5 pt-5">

		<div class="row mb-5 pb-lg-3 counters contact-us">

			<div class="col-lg-5 border-right pr-5">
				<h2 class="font-weight-bold text-9 text-white mb-0 appear-animation contact-h"  data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}">Contact Us</h2>
				<p class="text-5  negative-ls-05 pt-3 pb-4 mb-5 appear-animation text-white"  style="color: #fff !important;" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750">Need to make an inquiry? Or have issues or complaints? Please fill the form below and a representative will attend to you.</p>
			</div>
			<div class="col-lg-6 offset-lg-1 ">

				<form action="/contact" method="POST">

					@csrf

					@include('components.alert')

					<div class="row">
						<div class="form-group col-lg-12 mt-5">
							<input name="name" requried required type="text" class="form-control customise  pull-left mb-3 " style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" placeholder="Name">
						</div>
						<div class="form-group col-lg-6">
							<input name="email" requried type="text" class="form-control customise  pull-left mb-3 mr-5" style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" placeholder="Email">
						</div>
						<div class="form-group col-lg-6">
							<input name="phone" requried type="number" class="form-control customise  pull-left mb-3" style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" placeholder="Phone Number">
						</div>

						<div class="form-group col-lg-12">
							<textarea name="message" requried type="text" class="form-control customise pull-left mb-3" style=" border-radius: 4px; border-color: #fff; background-color: #FFFFFFFF" placeholder="Your Message"></textarea>
						</div>
						<button class="btn btn-danger wide-btn" type="submit">Submit</button>

					</div>
				</form>


			</div>
		</div>
	</div>
</section>
@endsection
