@extends('layouts.site')

@section('content')

<section class="section  section-concept sect section-no-border section-dark section-angled section-angled-reverse pt-5 m-0" >


	<div class="container first-container">

		<div class="row pb-lg-5">
			<div class="col-lg-4 text-center offset-lg-4">
				<h1 class="font-weight-bold text-9 text-color-dark mb-5 appear-animation animated appear-animation-visible fadeInUpShorter" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}" style="animation-delay: 100ms;">Popular</h1>
			</div>
		</div>

		<div class="row">

			<div class="col-md-4">
				<img src="/img/controlv2/instagram-blog.svg" class="img-fluid sc-icon" alt=""/>
				<article class="post post-medium border-0 pb-0 mb-5">
					<div class="post-image">
						<a href="blog-post.html">
							<img src="/img/blog/medium/blog-1.jpg" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
						</a>
					</div>

					<div class="post-content blue">

						<h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2"><a href="blog-post.html">Amazing Mountain</a></h2>
						<p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>

						<div class="post-meta">
							<span><i class="far fa-user"></i> 1 JUNE 2020</span>
							<span class="pull-right"><i class="far fa-comments"></i> 343</span>
						</div>

					</div>
				</article>
			</div>

			<div class="col-md-4">
				<img src="/img/controlv2/instagram-blog.svg" class="img-fluid sc-icon" alt=""/>
				<article class="post post-medium border-0 pb-0 mb-5">
					<div class="post-image">
						<a href="blog-post.html">
							<img src="/img/blog/medium/blog-2.jpg" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
						</a>
					</div>

					<div class="post-content white">

						<h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2"><a href="blog-post.html">Creative Business</a></h2>
						<p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>

						<div class="post-meta">
							<span><i class="far fa-user"></i> 1 JUNE 2020</span>
							<span class="pull-right"><i class="far fa-comments"></i> 343</span>
						</div>

					</div>
				</article>
			</div>

			<div class="col-md-4">
				<img src="/img/controlv2/twitter.svg" class="img-fluid sc-icon" alt=""/>
				<article class="post post-medium border-0 pb-0 mb-5">
					<div class="post-image">
						<a href="blog-post.html">
							<img src="/img/blog/medium/blog-3.jpg" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
						</a>
					</div>

					<div class="post-content black">

						<h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2"><a href="blog-post.html">Unlimited Ways</a></h2>
						<p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>

						<div class="post-meta">
							<span><i class="far fa-user"></i> 1 JUNE 2020</span>
							<span class="pull-right"><i class="far fa-comments"></i> 343</span>
						</div>

					</div>
				</article>
			</div>

			<div class="col-md-4">
				<img src="/img/controlv2/facebook.svg" class="img-fluid sc-icon" alt=""/>
				<article class="post post-medium border-0 pb-0 mb-5">
					<div class="post-image">
						<a href="blog-post.html">
							<img src="/img/blog/medium/blog-4.jpg" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
						</a>
					</div>

					<div class="post-content yellow">

						<h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2"><a href="blog-post.html">Developer Life</a></h2>
						<p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>

						<div class="post-meta">
							<span><i class="far fa-user"></i> 1 JUNE 2020</span>
							<span class="pull-right"><i class="far fa-comments"></i> 343</span>
						</div>
					</div>
				</article>
			</div>

			<div class="col-md-8">
				<img src="/img/controlv2/twitter.svg" class="img-fluid sc-icon" alt=""/>
				<article class="post post-medium border-0 pb-0 mb-5">
					<div class="post-image">
						<a href="blog-post.html">
							<img src="/img/blog/medium/blog-5.jpg" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
						</a>
					</div>

					<div class="post-content white">

						<h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2"><a href="blog-post.html">The Blue Sky</a></h2>
						<p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>

						<div class="post-meta">
							<span><i class="far fa-user"></i> 1 JUNE 2020</span>
							<span class="pull-right"><i class="far fa-comments"></i> 343</span>
						</div>
					</div>
				</article>
			</div>

			<div class="col-md-4">
				<img src="/img/controlv2/instagram-blog.svg" class="img-fluid sc-icon" alt=""/>
				<article class="post post-medium border-0 pb-0 mb-5">
					<div class="post-image">
						<a href="blog-post.html">
							<img src="/img/blog/medium/blog-6.jpg" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
						</a>
					</div>

					<div class="post-content lilac">

						<h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2"><a href="blog-post.html">Night Life</a></h2>
						<p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>

						<div class="post-meta">
							<span><i class="far fa-user"></i> 1 JUNE 2020</span>
							<span class="pull-right"><i class="far fa-comments"></i> 343</span>
						</div>

					</div>
				</article>
			</div>

			<div class="col-md-4">
				<img src="/img/controlv2/facebook.svg" class="img-fluid sc-icon" alt=""/>
				<article class="post post-medium border-0 pb-0 mb-5">
					<div class="post-image">
						<a href="blog-post.html">
							<img src="/img/blog/medium/blog-7.jpg" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
						</a>
					</div>

					<div class="post-content white">

						<h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2"><a href="blog-post.html">Another World Perspective</a></h2>
						<p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>

						<div class="post-meta">
							<span><i class="far fa-user"></i> 1 JUNE 2020</span>
							<span class="pull-right"><i class="far fa-comments"></i> 343</span>
						</div>

					</div>
				</article>
			</div>

			<div class="col-md-4">
				<img src="/img/controlv2/instagram-blog.svg" class="img-fluid sc-icon" alt=""/>
				<article class="post post-medium border-0 pb-0 mb-5">
					<div class="post-image">
						<a href="blog-post.html">
							<img src="/img/blog/medium/blog-8.jpg" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
						</a>
					</div>

					<div class="post-content green">

						<h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2"><a href="blog-post.html">The Creative Team</a></h2>
						<p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>

						<div class="post-meta">
							<span><i class="far fa-user"></i> 1 JUNE 2020</span>
							<span class="pull-right"><i class="far fa-comments"></i> 343</span>
						</div>

					</div>
				</article>
			</div>


		</div>

	</div>

</section>

@endsection