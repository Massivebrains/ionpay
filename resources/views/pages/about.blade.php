@extends('layouts.site')

@section('content')

<section class="section sect section-concept sect section-no-border section-dark section-angled section-angled-reverse pt-5" id="section-concept">


		<div class="container first-container">
			<div class="row counters">
				<div class="col-sm-8 col-lg-6 counter mb-md-0 beside-about">
					<img src="img/controlv2/about_bag.svg"  class="img-fluid imgsvg d-none d-lg-block appear-animation animated fadeInUp appear-animation-visible" alt="Screenshot 2" data-appear-animation="fadeInUp" data-appear-animation-delay="600" style="animation-delay: 600ms;">
				</div>
				<div class="col-sm-12 col-lg-6 counter mb-5 mb-md-0 about">
					<h1 class="font-weight-bold text-9 mb-0 appear-animation animated appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}">About</h1>

					<div class="appear-animation  board-about" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="750" data-appear-animation-duration="750">

						<p class="text-1rem text-color-default negative-ls-05 pt-3  appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750">
							Why limit your customers to just one payment option? iOnePay is a secure payment platform that does not require an internet connection to perform transactions.
							<br><br>
							Our platform allows vendors to receive payments from customers via different channels including SMS, Whatsapp, Facebook, Telegram, Twitter, Skype, Slack and other social media platforms. Shoppers do not need internet access to make payments anymore.
							<br><br>
							Simply input the unique product code and choose one of our channels to make your payment. No credit or debit card required.
						</p>
					</div>
					<div class="show-after"></div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row counters">

				<div class="col-sm-12 col-lg-7 counter mb-md-0 how">
					<h1 class="font-weight-bold text-9 mb-0 appear-animation animated appear-animation-visible how-title" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}">How to Use</h1>

					<div class="appear-animation board" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="750" data-appear-animation-duration="750">

						<p class="text-1rem text-color-default negative-ls-05 pt-3 pb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750">
							iOnePay generates unique codes for vendors and the different products they sell. These unique codes are then used to create order IDs that enable customers to shop the products attached to them.
							<br><br>
							Customers simply input the codes into the messaging platform, and an automatic response guides them through the buying process.


						</p>
					</div>
				</div>
				<div class="col-sm-8 col-lg-5 counter mb-md-0 beside-how">
					<img src="img/controlv2/how_phone.svg"  class="img-fluid imgsvg  appear-animation animated fadeInUp appear-animation-visible" alt="Screenshot 2" data-appear-animation="fadeInUp" data-appear-animation-delay="600" style="animation-delay: 600ms;">
				</div>
			</div>
		</div>

	</section>

{{--	<section class="section sect section-concept sect section-no-border  paywithOrderid pt-5 m-0" id="section-concept3" >--}}


{{--		<div class="container pb-5 pt-5">--}}

{{--			<div class="row mb-5 pb-lg-3 counters ">--}}

{{--				<div class="col-lg-4 ">--}}
{{--					<h2 class="font-weight-bold text-9 text-white mb-0 appear-animation" style="color: #fff !important;" data-appear-animation="fadeInUpShorter" data-appear-animation-duration="750" data-plugin-options="{'accY': -200}">Pay with Order ID</h2>--}}
{{--					<p class="text-5  negative-ls-05 pt-3 pb-lg-4 mb-lg-5 appear-animation text-white"  style="color: #fff !important;" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750">Input your Order ID below or select one of our channels to make your payment.</p>--}}
{{--				</div>--}}
{{--				<div class="col-lg-7 offset-lg-1">--}}
{{--					<p class="text-1rem negative-ls-05 pt-3 appear-animation green-online" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-appear-animation-duration="750" style="color: #68D873 !important;">Online Payment--}}
{{--						<form>--}}
{{--							<div class="row">--}}
{{--								<div class="col-lg-12">--}}
{{--									<input type="text" class="form-control customise long pull-left orderid-input" placeholder="Order ID number">--}}
{{--									<button class="btn btn-danger orderid-btn">Confirm Order</button>--}}
{{--								</div>--}}
{{--							</div>--}}
{{--						</form>--}}
{{--					</p>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--	</section>--}}

@endsection
