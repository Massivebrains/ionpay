<?php

namespace App\Services;

use Illuminate\Support\Str;
use \GuzzleHttp\Client;
use \GuzzleHttp\RequestOptions;
use \GuzzleHttp\Exception\ClientException;
use \GuzzleHttp\Exception\ConnectException;
use App\Services\ApiLogService;
use App\Traits\FulfilmentError;

class Ogaranya
{
    public static function get($url = '')
    {             
        $endpoint = 'https://admin.ogaranya.com/api/premium/'.ltrim($url, '/');

        if(env('APP_ENV') == 'local')
            $endpoint = 'http://localhost:7000/api/premium/'.ltrim($url, '/');

        try{

            $token    = session('api_token');

            $response = (new Client)->get($endpoint, [

                'headers' => [

                    'Accept'            => 'application/json',
                    'Authorization'     => "Bearer $token"
                ]
            ]);

            if((int)$response->getStatusCode() != 200){

                return response()->json(['status' => 'Failed', 'message' => 'We could not complete your data request.', 'data' => null], 503);
            }

            return json_decode($response->getBody()->getContents());

        }catch(ClientException $e){

            return json_decode($e->getResponse()->getBody()->getContents());

        }catch(ConnectException $e){

            return json_decode($e->getResponse()->getBody()->getContents());
            
        }catch(ServerException $e){

            return json_decode($e->getResponse()->getBody()->getContents());
        }
    }

    public static function post($url = '', $payload = [])
    {             
        $endpoint = 'https://admin.ogaranya.com/api/premium/'.ltrim($url, '/');
        
        if(env('APP_ENV') == 'local')
            $endpoint = 'http://localhost:7000/api/premium/'.ltrim($url, '/');

        try{

            $token    = session('api_token');

            $response = (new Client)->post($endpoint, [

                'headers' => [

                    'Accept'            => 'application/json',
                    'Authorization'     => "Bearer $token"
                ],

                RequestOptions::JSON => $payload
            ]);

            if((int)$response->getStatusCode() != 200){

                return response()->json(['status' => 'Failed', 'message' => 'We could not complete your data request.', 'data' => null], 503);
            }

            return json_decode($response->getBody()->getContents());

        }catch(ClientException $e){

            return json_decode($e->getResponse()->getBody()->getContents());

        }catch(ConnectException $e){
            
            return json_decode($e->getResponse()->getBody()->getContents());
            
        }catch(ServerException $e){

            return json_decode($e->getResponse()->getBody()->getContents());
        }
    }
}
