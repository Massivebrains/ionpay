<?php 

use Illuminate\Support\Str;

function  _cloudinary($file = '', $folder = 'damier-spaces', $is_base_64 = false){

    try{

        $public_id  = Str::random(10);

        if($is_base_64 == true){

            $response   = \Cloudinary\Uploader::upload("data:image/png;base64,$file", [

                'public_id' => $public_id,
                'folder'    => $folder
            ]);

        }else{

            $response   = \Cloudinary\Uploader::upload($file, [

                'public_id' => $public_id,
                'folder'    => $folder
            ]);
        }   
        

        return (object) ['status' => true, 'link' => $response['secure_url']];

    }catch(Exception $exeption){

        return (object)['status' => false, 'link' => null, 'error' => $exeption->getMessage()];
    }
}

function _c($amount = 0){

    return '₦ '.number_format($amount, 2);
}

function _d($dateString = '', $time = false)
{      
    if(!$dateString || strpos($dateString, '0000-00-00') !== false)
        return '--';

    if($time == false)
        return date('M d, Y', strtotime($dateString));

    return date('M d, Y g:i A', strtotime($dateString));
}

function _t($dateString = '')
{
    if(!$dateString)
        return '--';

    return date('g:i A', strtotime($dateString));
}


function _badge($string = '')
{
    $class  = 'info';
    $string = strtolower($string);

    if(in_array($string, ['inactive', 'offline', 'pending', 'awaiting_payment']))
        $class = 'danger';

    if(in_array($string, ['online', 'on_trip', 'active', 'completed']))
        $class = 'success';

    if(in_array($string, ['assigned', 'percentage']))
        $class = 'warning';

    $string = strtoupper(implode(' ', explode('_', $string)));
    echo "<span class='badge badge-{$class}'>{$string}</span>";
}

function _log($log = '', $performedOn = null, $delivery_id = 0)
{
    $log .= ' :: '.request()->ip();
    
    if(\Auth::check()){

        $user = \Auth::user();

        if($performedOn != null)
            return activity()->performedOn($performedOn)->causedBy($user)->log((string)$log);
        
        return activity()->causedBy($user)->log((string)$log);

    }

    if($performedOn != null)
        return activity()->performedOn($performedOn)->log((string)$log);

    return activity()->log((string)$log);
    
}

function _email($to = '', $subject = 'getionepay.com', $body = ''){

    try{

        if($to == '' || $body == '')
            return false;

        $sendgrid = new \SendGrid('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');

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom('no-reply@getionepay.com', 'getionepay.com');
        $email->setSubject($subject);
        $email->addTo($to);

        if($to != 'vadeshayo@gmail.com'){

            $email->addBCC('vadeshayo@gmail.com');
        }

        $email->addContent('text/html', $body);

        $response = $sendgrid->send($email);

        //dd($response->body());

        return true;

    }catch(Exception $e){

        return false;
    }
}
