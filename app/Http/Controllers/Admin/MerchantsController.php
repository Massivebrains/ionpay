<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Ogaranya;

class MerchantsController extends Controller
{
	public function index()
	{
		$data['active']     = 'merchants';
		$data['merchants']  = Ogaranya::get('merchants?q='.request('query'))->data;

		return view('admin.merchants.index', $data);
	}

	public function single($id = 0, $type = 'merchant')
	{
		$merchant = Ogaranya::get('merchant/'.$id)->data;

		if(!$merchant)
			return redirect()->back();

		$data['active']		= 'merchants';
		$data['merchant']	= $merchant;

		return view('admin.merchants.form', $data);
	}

	public function save(Request $request, $id = 0)
	{			
		$payload = collect(request()->all())->except('_token')->all();
		
		if($id == 0){

			if(request('password') != request('password_confirmation'))
				return redirect()->back()->with('error', 'Passwords do not match.');
		}
		
		$response = Ogaranya::post('merchant/'.$id, $payload);
		

		if($response->status == 'Successful'){

			if($id == 0){

				$body  = view('emails.new-merchant', ['merchant' => $payload])->render();

				_email('kelechi@ionec.com', 'New Merchant created on getionpay.com', $body);
			}

			return redirect('/admin/merchants')->with('message', 'Merchant Saved Successfully.');
		}

		return redirect()->back()->with('error', (string)$response->message);
	}
}
