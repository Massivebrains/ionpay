<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Ogaranya;

class ProductsController extends Controller
{
    public function index()
    {    	
    	$data['active']		= 'products';
    	$data['products'] 	= Ogaranya::get('products/product?q='.request('query'))->data;
    	$data['events'] 	= Ogaranya::get('products/event?q='.request('query'))->data;

    	return view('admin.products.index', $data);
    }

    public function single($id = 0, $type = 'product')
	{
		$product = Ogaranya::get('product/'.$id)->data;

		if(!$product)
			return redirect()->back();

		$data['active']		= 'products';
		$data['type']		= $product->id > 0 ? $product->product_type : $type;
		$data['product']	= $product;

		return view('admin.products.form', $data);
	}

	public function save(Request $request, $id = 0)
	{			
		$payload = collect(request()->all())->except('_token')->all();

		$response = Ogaranya::post('product/'.$id, $payload);
		
		if($response->status == 'Successful')
			return redirect('/admin/products')->with('message', 'Product Saved Successfully.');

		return redirect()->back()->with('error', (string)$response->data);
	}
}
