<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Ogaranya;

class HomeController extends Controller
{
    public function index()
    {
    	$data['active']		= 'dashboard';
    	
    	return view('admin.home.index', $data);
    }
}
