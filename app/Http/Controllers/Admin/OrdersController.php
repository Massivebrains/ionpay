<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\Services\Ogaranya;

class OrdersController extends Controller
{
    public function index()
    {
    	$data['active']     = 'orders';
        $data['orders']     = Ogaranya::get('orders?q='.request('query'))->data;

    	return view('admin.orders.index', $data);
    }

    public function single($id = 0)
    {
        $data['active']     = 'orders';
        $response           = Ogaranya::get('order/'.$id);
        
        if($response->status != 'Successful')
            return redirect('/admin/orders')->with('error', $respons->message);

        $data['order'] = $response->data;
        
        return view('admin.orders.order', $data);
    }
}
