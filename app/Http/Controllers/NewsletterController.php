<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $subject = 'A new newsletter subscription from ionpay.com';
        $body = 'Someone has registered for newsletter subscriptions. Email address: '.request('email');

        _email('mariam@ionec.com', $subject, $body);

        return redirect('/#newsletter')->with('message', 'Thank you for your subscription. You can unsubscribe anytime.');
    }
}
