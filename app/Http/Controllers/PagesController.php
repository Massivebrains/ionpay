<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
	public function contact(Request $request)
	{
		$this->validate($request, ['email' => 'required|email']);

		$subject = 'A new contact message from ionpay.com';
		$body = 'Someone just filled the contact form on ionpay.com - Find below details as specified. <br> <br>';
		$body.= '<strong>Email:</strong> '.request('email').'<br>';
		$body.= '<strong>Name:</strong> '.request('name').'<br>';
		$body.= '<strong>Phone Number:</strong> '.request('phone').'<br>';
		$body.= '<strong>Message:</strong> '.request('message').'<br>';

		_email('mariam@ionec.com', $subject, $body);

		return redirect('/#section-concept5')->with('message', 'Thank you for your message. We will get back to you shortly');
	}
}
