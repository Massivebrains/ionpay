<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InvitationCodeController extends Controller
{
    public function index(Request $request)
    {
        return redirect('/')->with('code_error', 'Invalid invitation Code.');
    }
}
