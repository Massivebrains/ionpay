<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \GuzzleHttp\Client;
use \GuzzleHttp\Exception\ClientException;

class BotsController extends Controller
{
	public function slack()
	{
		if(!request('code'))
			return view('bots.slack', ['message' => 'Code is required in query string.']);

		$error = $success = '';

		$code 			= request('code');
		$token 			= '2347030000449';
		$private_key 	= '8Mp5%7@P8RVkC9gpH4F&zRyPFUa*zzx2T*Hsd%hzT6&SsbqBfEqyRDu7kBNeXQWb3pr$Uj';

		$client = new \GuzzleHttp\Client([

			'headers' => [

				'token'         => $token,
				'publickey'     => hash('sha512', $token.$private_key)
			]
		]);

		try{

			$response = $client->request('GET', 'https://api.ogaranya.com/v1/ionepay/slack/auth/user/'.$code);

			$responseData = json_decode($response->getBody());

			if($response->getStatusCode() != 200 || optional($responseData)->status != 'success'){
				
				return view('bots.slack', ['message' => 'An Error occured. Please try again.']);

			}else{

				return view('bots.slack', ['message' => 'Slack Authorization was successful. Welcome to Ionpay!']);
			}

		}catch(ClientException $e){

			return view('bots.slack', ['message' => $e->getMessage()]);
		}
	}
}
