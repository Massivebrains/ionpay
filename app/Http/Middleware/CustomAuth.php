<?php

namespace App\Http\Middleware;

use Closure;

class CustomAuth
{
    public function handle($request, Closure $next)
    {
        if(!session('api_token') || !session('user'))
            return redirect('/login');

        return $next($request);
    }
}
