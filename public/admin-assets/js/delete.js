var baseURL = location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/admin";

function delete_news(s)
{
    if (confirm('Click OK if you really want to delete this News ')) {
        document.location.href = baseURL + '/delete_news/' + s;
    }
}

function delete_gallery(s)
{
    if (confirm('Click OK if you really want to delete this Gallery ')) {
        document.location.href = baseURL + '/delete_gallery/' + s;
    }
}

function delete_press(s)
{
    if (confirm('Click OK if you really want to delete this Press Release')) {
        document.location.href = baseURL + '/delete_press/' + s;
    }
}

function delete_board_member(s)
{
    if (confirm('Click OK if you really want to delete this Board Member')) {
        document.location.href = baseURL + '/delete_board_member/' + s;
    }
}

function delete_head_of_departments(s)
{
    if (confirm('Click OK if you really want to delete this Head of Department')) {
        document.location.href = baseURL + '/delete_head_of_departments/' + s;
    }
}

function delete_document(s)
{
    if (confirm('Click OK if you really want to delete this Document')) {
        document.location.href = baseURL + '/delete_document/' + s;
    }
}

function delete_movement_partners(s)
{
    if (confirm('Click OK if you really want to delete this Movement Partner')) {
        document.location.href = baseURL + '/delete_movement_partners/' + s;
    }
}

function delete_branch_profile(s)
{
    if (confirm('Click OK if you really want to delete this Branch Profile')) {
        document.location.href = baseURL + '/delete_branch_profile/' + s;
    }
}

function delete_newsletter(s)
{
    if (confirm('Click OK if you really want to delete this Newsletter')) {
        document.location.href = baseURL + '/delete_newsletter/' + s;
    }
}

