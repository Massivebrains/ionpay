<?php

use Illuminate\Support\Facades\Route;

Route::view('/', 'pages.home', ['active' => 'home']);
Route::view('/about', 'pages.about', ['active' => 'about', 'bg' => 'bg-blue']);
Route::view('/connect', 'pages.connect', ['active' => 'connect', 'bg' => 'bg-green']);
Route::view('/blog', 'pages.blog', ['active' => 'blog', 'bg' => 'bg-yellow']);
Route::view('/careers', 'pages.careers', ['active' => 'careers', 'bg' => 'bg-yellow']);
Route::view('/privacy-policy', 'pages.privacy-policy', ['active' => 'careers', 'bg' => 'bg-blue']);
Route::view('/terms-and-condition', 'pages.terms-and-condition', ['active' => 'careers', 'bg' => 'bg-green']);

Route::post('/contact', 'PagesController@contact');

Route::get('/login', 'AuthController@index')->name('login');
Route::post('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout');
Route::any('/forgot-password', 'AuthController@forgotPassword');
Route::any('/reset-password/{token?}', 'AuthController@resetPassword');

Route::post('/newsletter', 'NewsletterController@index');
Route::post('/invitation-code', 'InvitationCodeController@index');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'custom-auth'], function(){

	Route::get('/', 'HomeController@index');

	Route::get('/products', 'ProductsController@index');
	Route::get('/product/{id}/{type?}', 'ProductsController@single');
	Route::post('/product/{id}', 'ProductsController@save');

	Route::get('/orders', 'OrdersController@index');
	Route::get('/order/{id}', 'OrdersController@single');

	Route::get('/wallet', 'WalletController@index');
	Route::get('/customers', 'CustomersController@index');
	Route::get('/subscribers', 'CustomersController@subscribers');
	Route::get('/account', 'AccountController@index');
	Route::post('/account', 'AccountController@save');
	Route::get('/settings', 'AccountController@settings');
	Route::post('/settings/password', 'AccountController@password');
	Route::get('/merchants', 'MerchantsController@index');
	Route::get('/merchant/{id}', 'MerchantsController@single');
	Route::post('/merchant/{id}', 'MerchantsController@save');

});

Route::any('/bots/slack', 'BotsController@slack');
